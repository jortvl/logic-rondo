\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{8}{Semantics of First-Order Logic}{fol-semantics}

In this chapter, we will discuss the following two questions:
\begin{enumerate}
\item What are objects and their properties and when is a formula valid?
\item Are all derivable sequents valid entailments?
\end{enumerate}

The first question requires us to find a mathematical model of function and predicate symbols,
which we can extend to semantics of terms and formulas.
We will focus here on Boolean semantics for simplicity.
The second question will be answered by proving that both our proof systems \NDF{} and \cNDF{}
are sound with respect to the Boolean semantics.

Let us begin by finding out what an appropriate model of function and predicate symbols may be.

\section{Models of First-Order Logic}
\label{sec:fol-models}


Recall that in propositional logic a valuation $v \from \PVar \to \B$ on propositional variables
determined the truth value of formulas.
In first-order logic we need to provide interpretations instead for object variables, terms and
predicates.
Finding the right structures to do so is the subject of this section.

First, we recall some notation.
\begin{notation}{}{}
  Given a set $A$ and $n \in \N$, we write $\Tup{n}{A}$ for the $n$-fold \termdef{finite product}
  of $A$, consisting of $n$-tuples of elements in $A$:
  as follows.
  \begin{align*}
    \Tup{n}{A} = \setDef{(a_{1}, \dotsc, a_{n})}{a_{k} \in A \text{ for } k = 1, \dotsc, n}
  \end{align*}
  We identify $\Tup{1}{A}$ with $A$ for simplicity.
  Note also that $\Tup{0}{A}$ consists only of the single element $\unit$, which is a tuple with
  no entries.
\end{notation}

For example, the first few finite products look as follows.
\begin{align*}
  & \Tup{0}{A} = \set{\unit}
  & & \Tup{1}{A} = A
  & & \Tup{2}{A} = \setDef{(a,b)}{a,b \in A}
\end{align*}

\begin{definition}{}{model}
  Let $\Lang$ be a signature with $\Lang = (\FSym, \PSym, \ar)$.
  An \termdef{FO model}\todo{or $\Lang$-algebra or $\Lang$-structure} $\Model$ consists of
%  \begin{defbox}
    \begin{itemize}
    \item a non-empty set $U$, the \termdef{universe} of $\Model$
    \item for each $f \in \FSym$ a map
      \begin{equation*}
        \symInt{f} \from U^{\ar(f)} \to U
      \end{equation*}
    \item for each $P \in \PSym$ a predicate
      \begin{equation*}
        \symInt{P} \subseteq U^{\ar(P)}
      \end{equation*}
    \end{itemize}
%  \end{defbox}
  We also write $\Univ$ for $U$.
\end{definition}

Note that we require in \cref{def:model} the universe to be non-empty.
This restriction could in principle be dropped but that leads to some technical difficulties,
as there cannot be a model with an empty universe for a signature with constants.

Further, observe that if $c \in \FSym$ is a constant, then $\symInt{c}$ is a map of type
$U^{0} \to U$.
Since $U^{0}$ has one element, providing such a map corresponds to providing one element
$a \in U$:
\begin{equation*}
  \symInt{c}\unit = a
\end{equation*}

Let us unfold the definition for some specific arities.
%\begin{example}{}{generic-model}
  Suppose our signature $\Lang$ consists of three function symbols $c$, $f$ and $g$ with arity
  $0$, $1$ and $2$, respectively, and three predicate symbols $P$, $Q$, $R$ also of arity $0$, $1$
  and $2$.
  An $\Lang$-model $\Model$ consists of a universe $U$, maps and predicates as in
%  the following table.
  \cref{tab:model-data}.

  We see that $\symInt{c}$ denotes one element $\symInt{c}(\ast) \in U$, as discussed above;
  $\symInt{f}$ is a unary map; and $\symInt{g}$ is a binary map.
  Moreover, we find that $\symInt{P}$ is either the empty set $\emptyset$
  or the singleton set $U^{0}$.
  In other words, $P$ is nothing but a \emph{propositional variable}!
  Finally, $\symInt{Q}$ is a predicate, or unary relation, while $\symInt{R}$ is a binary relation.
% \end{example}
  \begin{table}[ht]
    \centering
  % \begin{center}
    \renewcommand*{\arraystretch}{1.3}
    % \tabulinesep=1.0mm
    \begin{exbox}
    \begin{tabular}{lc|c|l}
                                         & Symbol s & $\ar(s)$ & Interpretation type \\
                                         \hline
      \multirow{3}{*}{Function Symbols}  & $c$      & 0        & $\symInt{c} \from U^{0} \to U$ \\
                                         & $f$      & 1        & $\symInt{f} \from U \to U$ \\
                                         & $g$      & 2        & $\symInt{g} \from U^{2} \to U$ \\
                                         \hline
      \multirow{3}{*}{Predicate Symbols} & $P$      & 0        & $\symInt{P} \subseteq U^{0}$ \\
                                         & $Q$      & 1        & $\symInt{Q} \subseteq U$ \\
                                         & $R$      & 2        & $\symInt{R} \subseteq U^{2}$
    \end{tabular}
    \end{exbox}
  % \end{center}
    \caption{Data of a model for the indicated signature}
    \label{tab:model-data}
  \end{table}

\begin{quiz}
  Let $\Lang = (\emptyset, \PSym, \ar)$ with $\PSym = \set{P}$ and $\ar(P) = 0$.
  How many possibilities are there to make a model for $\Lang$?
\end{quiz}
\begin{answer}
  There are exactly two possibilities because $\symInt{P}$ must be a subset of $\Tup{0}{A}$.
  Thus, either $\symInt{P} = \emptyset$ or $\symInt{P} = \Tup{0}{A}$.
\end{answer}

In the next example, we discuss a signature and two models that occur ``in the wild''.
\begin{example}{}{arithmetic-model}
  Let $\Lang$ be given as follows.
  \begin{gather*}
    \FSym = \set{\zero, \one, p}
    \qquad \PSym = \set{I,L} \\
    \ar(\zero) = \ar(\one) = 0
    \qquad \ar(p) = \ar(I) = \ar(L) = 2
  \end{gather*}
  The interpretation of this signature could be that $\zero$ and $\one$ stand for the numbers
  $0$ and $1$,
  $p$ for addition (plus), $I$ for equality (identity), and $L$ for less-than.
  Indeed, we can give such an interpretation, which leads to the model $\ArithM$ in
  \cref{tab:arithmetic-model} with universe $\ArithU = \N$.
\end{example}
  \begin{table}[ht]
    \centering
    \renewcommand*{\arraystretch}{1.3}
    \begin{exbox}
    \begin{tabular}{c|l|l}
      Symbol & Interpretation type & Interpretation \\
      \hline
      $\zero$  & $\arithInt{\zero} \from \N^{0} \to \N$  & $\arithInt{\zero}\unit = 0$ \\
      $\one$   & $\arithInt{\one} \from \N^{0} \to \N$   & $\arithInt{\one}\unit = 1$ \\
      $p$      & $\arithInt{p} \from \N^{2} \to \N$ & $\arithInt{p}(n, m) = n + m$ \\
      \hline
      $I$      & $\arithInt{I} \subseteq \N^{2}$  & $\arithInt{I} = \setDef{(n,m)}{n = m}$ \\
      $L$      & $\arithInt{L} \subseteq \N^{2}$  & $\arithInt{L} = \setDef{(n,m)}{n \leq m}$
    \end{tabular}
  \end{exbox}
    \caption{Arithmetic model $\ArithM$ over universe $\N$}
    \label{tab:arithmetic-model}
  \end{table}

It is not necessary to interpret $\Lang$ as in \cref{ex:arithmetic-model} and we can give different
meanings to the symbols and even the universe.
\begin{example}{}{language-model}
  We show in this example how to use the same signature $\Lang$ as in \cref{ex:arithmetic-model} to reason
  about formal languages, where we use the notation introduced in \cref{sec:formal-languages}:
  The function symbols $\zero$, $\one$ and $p$ correspond to, respectively, the empty language,
  the language containing only the empty and union of languages.
  The predicate symbols, on the other hand, can be interpreted as language equality and language
  inclusion.
  All of this is summed up in \cref{tab:language-model}.
  Note that that $\zero$ and $p$ behave similarly to $0$ and addition under this interpretation.
  We will discuss this later in more depth.
\end{example}
  \begin{table}[ht]
    \centering
    \renewcommand*{\arraystretch}{1.3}
    \begin{exbox}
    \begin{tabular}{c|l|l}
      Symbol & Interpretation type & Interpretation \\
      \hline
      $\zero$ & $\langInt{\zero} \from \Langs{A}^{0} \to \Langs{A}$
      & $\langInt{\zero}\unit = \emptyset$ \\
      $\one$ & $\langInt{\one} \from \Langs{A}^{0} \to \Langs{A}$
      & $\langInt{\one}\unit = \set{\eword}$ \\
      $p$ & $\langInt{p} \from \Langs{A}^{2} \to \Langs{A}$
      & $\langInt{p}(L_{1}, L_{2}) = L_{1} \cup L_{2}$ \\
      \hline
      $I$ & $\langInt{I} \subseteq \Langs{A}^{2}$
      & $\langInt{I} = \setDef{(L_{1}, L_{2})}{L_{1} =  L_{2}}$ \\
      $L$ & $\langInt{L} \subseteq \Langs{A}^{2}$
      & $\langInt{L} = \setDef{(L_{1}, L_{2})}{L_{1} \subseteq  L_{2}}$
    \end{tabular}
  \end{exbox}
    \caption{Language model $\LangM$ for $\Lang$ over universe $\Langs{A}$}
    \label{tab:language-model}
  \end{table}

Clearly, the two models $\ArithM$ and $\LangM$ in \cref{ex:arithmetic-model} and
\cref{ex:language-model} are completely different interpretations of $\Lang$.
This illustrates the power of first-order logic: one language we can reason about an enormous
variety of different structures.
However, as we will in \cref{chap:fol-limits}, this power can also become a weakness of
first-order logic.

\todo[inline]{Give graph and robot example}

\section{Valuations and the Interpretion of FOL}
\label{sec:valuations-interpretation-fol}

Just as propositional variables in propositional logic, the object variables in first-order formulas
have no intrinsic meaning.
Instead, we have to give meaning to them externally through valuations, which assign to each
variable an element of a given universe.\todo{Say something about Tarski}

\begin{definition}{}{valuations}
  Given a signature $\Lang$ and an $\Lang$-model $\Model$, an \notion{$\Model$-valuation},
  or simply \notion{valuation}, is a map $v$ of the following type.
  \begin{emphdef}{equation*}
     v \from \Var \to \Univ
   \end{emphdef}
\end{definition}

With interpretations of variables at our disposal, we can understand also the meaning of terms.
\begin{definition}{}{term-semantics}
  A valuation $v$ in a model $\Model$ extends to the \notion{semantics of terms}
  $\sem{-}^{\Model}_{v} \from \Term \to \Univ$ by iteration on terms as follows.
  \begin{emphdef}{align*}
    \sem{x}^{\Model}_{v} & = v(x) \\
    \sem{c}^{\Model}_{v} & = \symInt{c}\unit \\
    \sem{f(t_1, \dotsc, t_n)}^{\Model}_{v}
    & = \symInt{f} \parens*{\sem{t_1}^{\Model}_{v}, \dotsc, \sem{t_n}^{\Model}_{v}}
  \end{emphdef}
  If $\Model$ is clear from the context, then we just write $\sem{-}_{v}$ instead of
  $\sem{-}^{\Model}_{v}$.
\end{definition}

Let us demonstrate valuations and the term semantics for the models in
\cref{ex:arithmetic-model,ex:language-model}.
\begin{example}{}{arithmetic-valuation}
  We begin with the arithmetic model $\ArithM$ from \cref{ex:arithmetic-model}.
  Let $x \in \Var$ be some variable and define
  \begin{equation*}
    v(y) =
    \begin{cases}
      5, & x = y \\
      0, & x \ne y
    \end{cases}
  \end{equation*}
  Under this valuation, the term $p(\zero, p(x, \one))$ gets the following semantics assigned.
  \begin{align*}
    \sem{p(\zero, p(x, \one))}_{v}
    & = \sem{\zero}_{v} + \sem{p(x, \one)}_{v} \\
    & = \sem{\zero}_{v} + (\sem{x}_{v} + \sem{\one}_{v}) \\
    & = \arithInt{\zero}\unit + (v(x) + \arithInt{\one}\unit) \\
    & = 0 + (5 + 1) \\
    & = 6
  \end{align*}
\end{example}

The next example provides semantics for the same term but in the language model.
\begin{example}{}{language-valuation}
  Let $x \in \Var$ be some variable and define
  \begin{equation*}
    v(y) =
    \begin{cases}
      A, & x = y \\
      \emptyset, & x \ne y
    \end{cases}
  \end{equation*}
  Under this valuation, the term $p(\zero, p(x, \one))$ gets the following semantics assigned.
  \begin{align*}
    \sem{p(\zero, p(x, \one))}_{v}
    & = \sem{\zero}_{v} \cup \sem{p(x, \one)}_{v} \\
    & = \sem{\zero}_{v} \cup (\sem{x}_{v} \cup \sem{\one}_{v}) \\
    & = \langInt{\zero}\unit \cup (v(x) \cup \langInt{\one}\unit) \\
    & = \emptyset \cup (A \cup \set{\eword}) \\
    & = \setDef{w \in \Words{A}}{\wlen(w) \leq 1}
  \end{align*}
  Here, $\wlen(w)$ is the length of the word $w$.
\end{example}

Now that we have an interpretation of terms, we can further extend it to an interpretation
on formulas.
Recall that we needed to update substitutions to eliminate universal quantifiers and introduce
existential quantifiers.
A similar operation on valuations is necessary in the semantics of FOL formulas.

\begin{definition}{}{update-valuation}
  Let $v$ be an $\Model$ valuation, $x \in \Var$ and $a \in \Univ$.
  We define the \notion{update of valuations} on $v$ by the following equation.
  \begin{emphdef}{equation*}
    (v \updateV{x}{a})(y) =
    \begin{cases}
      a, & y = x \\
      v(y), & y \ne x
    \end{cases}
  \end{emphdef}
\end{definition}

Let us briefly illustrate the update of valuations.
\begin{example}{}{}
  We start with the valuation $v \from \Var \to \N$ given by
  $v(y) = 0$ for all $y \in \Var$.
  Given variables $x, z \in \Var$ with $x \ne z$, we have
  \begin{align*}
    & (v\updateV{x}{1})(x) & = 1 \\
    & (v\updateV{x}{1})(z) & = 0 \\
    & (v\updateV{x}{1}\updateV{z}{2})(x) & = 1 \\
    & (v\updateV{x}{1}\updateV{z}{2})(z) & = 2 \\
    & (v\updateV{x}{1}\updateV{z}{2}\updateV{x}{3})(x) & = 3 \\
    & (v\updateV{x}{1}\updateV{z}{2}\updateV{x}{3})(z) & = 2
  \end{align*}
\end{example}

The update operation of valuations allows us now to give semantics to
quantifiers and thereby to all formulas.
\begin{definition}{}{formula-semantics}
  Let $\Lang$ be a signature and $\Model$ an $\Lang$-model.
  We define for all $\Model$-valuations $v$ a map
  \begin{emphdef}{equation*}
    \sem{-}_{v} \from \Forms \to \B
  \end{emphdef}
  by iteration on formulas.
  \begin{emphdef}{align*}
    \sem{\bot}_v & = 0 \\
    \sem{P(t_1, \dotsc, t_n)}_v & =
    \begin{cases}
      1, & (\sem{t_1}_{v}, \dotsc, \sem{t_n}_v) \in \symInt{P} \\
      0, & \text{otherwise}
    \end{cases}
    \\
    \sem{\phi \land \psi}_v & = \min \set{\sem{\phi}_v, \sem{\psi}_v} \\
    \sem{\phi \lor \psi}_v & = \max \set{\sem{\phi}_v, \sem{\psi}_v} \\
    \sem{\phi \to \psi}_v & = \sem{\phi}_v \implies \sem{\psi}_v \\
    \sem{\all{x} \phi}_v & = \min \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ} \\
    \sem{\exist{x} \phi}_v & = \max \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}
  \end{emphdef}
\end{definition}

First of all, note that \cref{def:formula-semantics} is correct for quantifiers because
renaming of variables does not affect the result:
\begin{equation*}
  \sem{\all{x} \phi}_v = \sem{\all{y} \appS{\phi}{\update{x}{y}}}_v
  \text{ for all } \fresh{y}{\phi}
\end{equation*}
because
\begin{equation*}
  \min \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}
  = \min \setDef{\sem{\appS{\phi}{\update{x}{y}}}_{v \updateV{y}{a}}}{a \in \Univ}
\end{equation*}
Thus, the definition of semantics is compatible with the equality of formulas that we have
required to hold in \cref{chap:fol-proofs}.

Next, note that the minimum and maximum are always taken over a non-empty subset of $\B$.
The possibilities that arise in the semantics of quantifiers are summed up in
\cref{tab:quantifier-semantics} together with the corresponding quantification.
\begin{table}[ht]
  \centering
  \begin{tabular}{c|c|c|l}
    $\setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}$ & $\sem{\all{x} \phi}_{v}$
    & $\sem{\exist{x} \phi}_{v}$
    & Interpretation \\
    \hline
    $\set{0}$   & 0 & 0 & $\phi$ holds for no $a$ \\
    $\set{1}$   & 1 & 1 & $\phi$ holds for all $a$ \\
    $\set{0,1}$ & 0 & 1 & $\phi$ holds for some $a$
  \end{tabular}
  \caption{Possibilities for quantifier semantics}
  \label{tab:quantifier-semantics}
\end{table}
Note that if we would allow the universe to be empty, then we would have a fourth
option in the table, in which the universal quantifier would be always true
and the existential quantifier always false.

Let us now calculate the truth value of some formulas.
\begin{example}[breakable]{}{arithmetic-semantics}
  Recall the signature $\Lang$ and arithmetic model $\ArithM$ from \cref{ex:arithmetic-model}.
  Consider the formula
  \begin{equation*}
    \all{x} I(p(x, \zero), x)
  \end{equation*}
  that expresses under the arithmetical interpretation that $x + 0 = x$.
  In other words, the formula should be true for any valuation over $\ArithM$.
  Indeed, given a valuation $v$ and a natural number $n$, we have
  \begin{equation*}
    \sem{p(x, \zero)}_{v \updateV{x}{n}} = 0 + n = n
  \end{equation*}
  and thus
  \begin{align*}
    \sem{I(p(x, \zero), x)}_{v \updateV{x}{n}}
    & =
    \begin{cases}
      1, & \parens*{\sem{p(x, \zero)}_{v \updateV{x}{n}}, \sem{x}_{v \updateV{x}{n}}} \in \arithInt{I} \\
      0, & \text{otherwise}
    \end{cases}
    \\
    & =
    \begin{cases}
      1, & n = n \\
      0, & \text{otherwise}
    \end{cases}
    \\
    & = 1.
  \end{align*}
  As expected, this gives us
  \begin{align*}
    \sem{\all{x} I(p(x, \zero), x)}_{v}
    & = \min \setDef{\sem{I(p(x, \zero), x)}_{v \updateV{x}{n}}}{n \in \N} \\
    & = \min \set{1} \\
    & = 1
  \end{align*}
  and thus $\all{x} I(p(x, \zero), x)$ is true in $\ArithM$.

  As a second example, consider the formula
  \begin{equation*}
    \all{x} \exist{y} L(x, y) \land \neg I(x,y)
  \end{equation*}
  that states that for every number there is a strictly larger number.
  This formula is true in the arithmetic model because for every $n \in \N$
  we have that $n < n + 1$.
  In other words, for every valuation $v$ and $n \in \N$
  \begin{equation*}
    \sem{L(x, y) \land \neg I(x,y)}_{v \updateV{x}{n} \updateV{y}{n+1}} = 1.
  \end{equation*}
  Thus,
  \begin{align*}
    & \sem{\exist{y} L(x, y) \land \neg I(x,y)}_{v \updateV{x}{n}} \\
    & = \max \setDef{\sem{L(x, y) \land \neg I(x,y)}_{v\updateV{x}{n} \updateV{y}{m}}}{m \in \N} \\
    & = 1
    \tag*{because $n+1 \in \N$}
  \end{align*}
  and
  \begin{align*}
    & \sem{\all{x} \exist{y} L(x, y) \land \neg I(x,y)}_{v} \\
    & = \min \setDef{\sem{\exist{y} L(x, y) \land \neg I(x,y)}_{v\updateV{x}{n}}}{n \in \N} \\
    & = \min \set{1} \\
    & = 1
  \end{align*}
\end{example}

\begin{quiz}
  Is the formula $\all{x} \exist{y} L(x, y) \land \neg I(x,y)$ from \cref{ex:arithmetic-semantics}
  true in the language model $\LangM$?
\end{quiz}
\begin{answer}
  The formula $\all{x} \exist{y} L(x, y) \land \neg I(x,y)$ translates in the language
  model $\LangM$ to ``for every language $U$ there is language $V$ that strictly contains $U$:
  $U \subset V$.''
  This is not true because the total language $\Words{A}$ is maximal.
  Indeed, formally we have for all valuations $v$ and $V \subseteq \Words{A}$ with
  $w = v \updateV{x}{\Words{A}}$ that
  \begin{equation*}
    \sem{L(x, y) \land \neg I(x,y)}_{w \updateV{y}{V}} =
    \begin{cases}
      1, & \Words{A} \subseteq U \text{ and } \Words{A} \neq V \\
      0, & \text{otherwise}
    \end{cases}
    = 0.
  \end{equation*}
  This gives $\sem{\exist{y} L(x, y) \land \neg I(x,y)}_{w} = 0$
  and thus
  \begin{align*}
    & \sem{\all{x} \exist{y} L(x, y) \land \neg I(x,y)}_{v} \\
    & = \min\setDef{
      \sem{\exist{y} L(x, y) \land \neg I(x,y)}_{v \updateV{x}{U}}
    }{U \in \Words{A}} \\
    & \leq \sem{\exist{y} L(x, y) \land \neg I(x,y)}_{w} \\
    & = 0
  \end{align*}
\end{answer}

\section{Entailment and Satisfiability for FOL}
\label{sec:entailment}

In \cref{sec:valuations-interpretation-fol}, we have defined the functional interpretation of
first-order formulas in terms of the mapping $\sem{-}$.
As for propositional logic, we can also give a relational interpretation of formulas.
This allows us to easily define satisfiability and tautologies.

\begin{definition}{}{validity}
  Let $\phi$ be an $\Lang$-formula and $\Gamma$ a set of $\Lang$-formulas.
  We define the \notion{semantic entailment}, where
  $\sem{\Gamma}_{v} = \min \setDef{\sem{\psi}_{v}}{\psi \in \Gamma}$, as follows.
%  \begin{defbox}[before skip=2mm]
  \begin{center}
    \begin{tabular}{rll}
      % $\Model, v \entails \phi$ & if $\sem{\phi}^{\Model}_{v} = 1$
      % & ($\Model$ and $v$ \notion{satisfy} $\phi$) \\
      $\Gamma \entails_{\Model} \phi$
      &if $\sem{\Gamma}^{\Model}_{v} \leq \sem{\phi}^{\Model}_{v}$ for all valuations $v$
      & ($\Gamma$ \notion{entails} $\phi$ in $\Model$) \\
      $\Gamma \entails \phi$
      & if $\sem{\Gamma}^{\Model}_{v} \leq \sem{\phi}^{\Model}_{v}$
      & ($\Gamma$ \notion{entails} $\phi$) \\
      & for all models $\Model$ and valuations $v$ &
    \end{tabular}
  \end{center}
  % \end{defbox}
  We say that $\Model$ and $v$ \notion{satisfy} $\phi$ if $\sem{\phi}^{\Model}_{v} = 1$,
  and that $\phi$ is \notion{satisfiable}, if there is a model $\Model$ and an
  $\Model$-valuation $v$ that satisfy $\phi$.
  The formula $\phi$ is a \notion{tautology}, written $\entails \phi$,
  if $\emptyset \entails \phi$.
  Finally, we say that $\Model$ \notion{validates} $\phi$, if $\emptyset \entails_{\Model} \phi$,
  that is, $\phi$ is a tautology only within the model $\Model$.
\end{definition}

Let us give some examples in the arithmetic model.
\begin{example}{}{arithmetic-entailment}
  Let $\phi_{e}$ be the formula $\exist{y} I(x, p(y, y))$ (``$x$ is even''),
  and let $v_{1}$ and $v_{2}$ be given as follows.
  \begin{equation*}
    v_{1}(z) =
    \begin{cases}
      2, & z = x \\
      1, & z \ne x
    \end{cases}
    \qquad
    v_{2}(z) =
    \begin{cases}
      3, & z = x \\
      1, & z \ne x
    \end{cases}
  \end{equation*}
  Then $\ArithM$ and $v_{1}$ satisfy $\phi_{e}$, but $\ArithM$ and $v_{2}$ do not.
  Therefore $\phi_{e}$ is satisfiable but not validated by $\ArithM$.

  Let $\Gamma = \set{\phi_{e}}$ and $\phi_{o} = \exist{y} I(x, p(p(y, y), \one))$.
  Then
  \begin{equation*}
    \Gamma \entails_{\ArithM} \appS{\phi_{o}}{\update{x}{p(x, \one)}}
  \end{equation*}
  holds:
  If $\sem{\Gamma}_{v}^{\ArithM} = 1$, then $v(x)$ must be an even number.
  Thus, $v(x) + 1$ is an odd number and $\sem{\appS{\phi_{o}}{\update{x}{p(x, \one)}}}_{v} = 1$.
  Formally,
  \begin{align*}
    & \sem{\Gamma}_{v} = 1 \\
    & \text{iff } \sem{\phi_{e}}_{v} = 1 \\
    & \text{iff } \min \setDef{\sem{I(x, p(y, y))}_{v \updateV{y}{n}}}{ n \in \N} = 1 \\
    & \text{iff } \sem{I(x, p(y, y))}_{v \updateV{y}{n}} = 1 \text{ for some } n \in \N
    \tag{see \cref{tab:quantifier-semantics}} \\
    & \text{iff } v(x) = n + n \text{ for some } n \in \N \\
    & \text{iff } v(x) \text{ even }
  \end{align*}
  and under this assumption
  \begin{align*}
    \sem{\phi_{o} \update{x}{p(x, \one)}}_{v}
    & = \sem{(\exist{y} I(x, p(p(y, y), \one)))\update{x}{p(x, \one)}}_{v} \\
    & = \sem{\exist{y} I(p(x, \one), p(p(y, y), \one))}_{v} \\
    & = \max \setDef{\sem{I(p(x, \one), p(p(y, y), \one))}_{v \updateV{y}{n}}}{n \in \N} \\
    & \geq \sem{I(p(x, \one), p(p(y, y), \one))}_{v \updateV{y}{v(x)/2}} \\
    & = 1,
  \end{align*}
  where we use the identity $v(x) + 1 = (v(x)/2 + v(x)/2) + 1$ in the last line.
\end{example}

To show that a formula is not a tautology, it can be convenient or even necessary to choose
the interpretation of function or predicate symbols appropriately, as the following example shows.
\begin{example}{}{arithmetic-sat-taut}
  We have seen in \cref{ex:arithmetic-semantics} that
  $\sem{\all{x} I(p(x, \zero), x)}^{\ArithM}_{v} = 1$ for any valuation $v$
  and therefore $\ArithM$ validates $\all{x} I(p(x, \zero), x)$.
  However, this formula is not a tautology because it is not satisfied by the model $\Model$
  with $\Univ = \N$, $\symInt{\zero} = 1$ and otherwise the same interpretation as in
  the arithmetic model $\ArithM$.
\end{example}

In \cref{chap:fol-proofs}, we have seen formulas that were derivable in \NDF{}, which are a good
source of tautologies, see also \cref{sec:nd-sound} below.
\begin{example}{}{all-to-not-exist-tautology}
  We claim that the formula $(\all{x} \phi) \to \neg \exist{x} \neg \phi$ is a tautology for
  any formula $\phi$.
  Indeed, let $\Model$ be a model for the signature $\Lang$, over which $\phi$ is a formula,
  and $v$ a valuation in $\Model$.
%  The following calculation shows that $\sem{\psi}_{v} = 1$.
  We obtain from \cref{tab:quantifier-semantics} that
  % \begin{equation*}
  %   \sem{\all{x} \phi}_{v} = 1
  %   \text{ iff } \sem{\phi}_{v \updateV{x}{a}} \text{ for all } a \in \Univ.
  % \end{equation*}
  % and
  \begin{align*}
    \sem{\neg \exist{x} \neg \phi}_{v} = 1
    & \text{ iff } \sem{\exist{x} \neg \phi}_{v} = 0 \\
    & \text{ iff } \sem{\neg \phi}_{v \updateV{x}{a}} = 0  \text{ for all } a \in \Univ \\
    & \text{ iff } \sem{\phi}_{v \updateV{x}{a}} = 1 \text{ for all } a \in \Univ \\
    & \text{ iff } \sem{\all{x} \phi}_{v} = 1
  \end{align*}
  This implies that
  \begin{equation*}
    \sem{\all{x} \phi}_{v} \leq \sem{\neg \exist{x} \neg \phi}_{v}
  \end{equation*}
  and thus
  \begin{align*}
    & \sem{(\all{x} \phi) \to \neg \exist{x} \neg \phi}_{v} \\
    & = \sem{(\all{x} \phi)}_{v} \implies \sem{\neg \exist{x} \neg \phi}_{v} \\
    & = 1.
  \end{align*}
  Hence, $(\all{x} \phi) \to \neg \exist{x} \neg \phi$ is a tautology.

  It should be noted that implication and entailment are, like in propositional logic,
  closely related.
  In this example, we have $\all{x} \phi \entails \neg \exist{x} \neg \phi$ and
  the proof proceeds in essentially the same way:
  First, we have that
  $\all{x} \phi \entails \neg \exist{x} \neg \phi$
  holds if and only if
  $\sem{\all{x} \phi}_{v} \leq \sem{\neg \exist{x} \neg \phi}_{v}$ for all models and
  valuations.
  If $\sem{\all{x} \phi}_{v} = 0$, we have nothing to prove, while if
  $\sem{\all{x} \phi}_{v} = 1$ we use the reasoning above to obtain that
  $\sem{\neg \exist{x} \neg \phi}_{v} = 1$.
  Therefore, $\sem{\all{x} \phi}_{v} \leq \sem{\neg \exist{x} \neg \phi}_{v}$
  holds for all models and valuations and
  $\all{x} \phi \entails \neg \exist{x} \neg \phi$.
\end{example}

\todo[inline]{
  Add this:

Suppose that we want to show that a closed formula, a formula $\phi$ without free variables,
is satisfied by a model or is a tautology.
We have seen in the previous examples that we then have to prove for any valuation $v$ that
$\sem{\phi}_{v} = 1$.
This can be very tedious.
The following theorem provides a simplification and allows us to reason without explicitly
referring to valuations.
\begin{theorem}{}{fol-entailment-recursive}
  Let
\end{theorem}
}

\todo[inline]{Add extensionality}
% We end this section with a result that tells us that the semantics of formulas is determined
% by the

\section{Soundness of Natural Deduction for FOL}
\label{sec:nd-sound}

We come now to the second initial question:
Are all derivable formulas tautologies?
Using \cref{def:validity}, we can state this question precisely by asking: if there is a proof for
the sequent $\seq{\Gamma}{\phi}$ in one of the systems from \cref{chap:fol-proofs}, is $\phi$ then
entailed semantically by $\Gamma$?
The answer to this question is the main result of this chapter.

\begin{theorem}{Soundness}{fol-soundness}
  For every formula $\phi$ and list of formulas $\Gamma$ over a signature $\Lang$
    \begin{enumerate}
    \item if \ $\seq{\Gamma}{\phi}$ is derivable in \NDF{}, then
      $\Gamma \entails \phi$.
    \item if \ $\seq{\Gamma}{\phi}$ is derivable in \cNDF{}, then
      $\Gamma \entails \phi$.
    \end{enumerate}
\end{theorem}

Instantiating \cref{thm:fol-soundness} with the empty list of assumptions, we obtain
the following corollary.
\begin{corollary}{}{fol-soundness-tautology}
  For every formula $\phi$ over a signature $\Lang$ the following holds.
    \begin{enumerate}
    \item If \ $\seq{}{\phi}$ is derivable in \NDF{}, then $\phi$ is a tautology.
      % $\entails \phi$.
    \item If \ $\seq{}{\phi}$ is derivable in \cNDF{}, then $\phi$ is a tautology.
      % $\entails \phi$.
    \end{enumerate}
\end{corollary}

The most important use of \cref{thm:fol-soundness} is to show that a formula is not provable.
\begin{example}{}{fol-unprovable}
  Recall from \cref{ex:arithmetic-sat-taut} that $\all{x} I(p(x, \zero), x)$
  is not a tautology.
  Thus, this formula cannot be provable in \NDF{}, as we would obtain a contradiction
  with \cref{cor:fol-soundness-tautology}.
\end{example}

The proof of \cref{thm:fol-soundness} requires some interesting results that
essentially show that substitutions are the syntactic counterpart of valuations.
This is proved below in \cref{lem:substitution-to-valution-on-terms} for terms
and in \cref{lem:substitution-to-valution-on-formulas} for formulas.

\begin{lemma}{}{substitution-to-valution-on-terms}
  For all substitutions $\sigma$, terms $t$, valuations $v$ and variables $x$,
  where $x$ is fresh for $\sigma$, the following equation holds.
  \begin{equation*}
    \sem{\appS{t}{\sigma}}_{v}
    = \sem{\appS{t}{(\sigma \update{x}{x})}}_{v \updateV{x}{\sem{\sigma(x)}_{v}}}
  \end{equation*}
\end{lemma}
\begin{proof}
  We let $\sigma' = \sigma \update{x}{x}$ and $v' = v \updateV{x}{\sem{\sigma(x)}_{v}}$, which
  means that we have to prove
  \begin{equation*}
    \sem{\appS{t}{\sigma}}_{v} = \sem{\appS{t}{\sigma'}}_{v'}
  \end{equation*}

  This proof proceeds by induction on the term $t$.
  \begin{itemize}
  \item In the base case we have for a variable $y$:
    \begin{align*}
      \sem{\appS{y}{\sigma}}_{v}
      & = \sem{\sigma(y)}_{v}
      \tag*{def. substitution}
      \\
      & =
      \begin{cases}
        \sem{\sigma(x)}_{v}, y = x \\
        \sem{\sigma(y)}_{v}, y \ne x
      \end{cases}
      \\
      & =
      \begin{cases}
        \sem{\change{x}}_{\change{v'}}, y = x \\
        \sem{\sigma(y)}_{v}, y \ne x
      \end{cases}
      \tag*{def. $v'$} \\
      & =
      \begin{cases}
        \sem{x}_{v'}, y = x \\
        \sem{\sigma(y)}_{\change{v'}}, y \ne x
      \end{cases}
      \tag*{$x$ fresh for $\sigma$} \\
      & = \sem{\appS{y}{\sigma'}}_{v'}
      \tag*{def. substitution update}
    \end{align*}
  \item In the induction step, we have
    \begin{align*}
      & \sem{\appS{f(t_{1}, \dotsc, t_{n})}{\sigma}}_{v} \\
      & = \sem{f(\appS{t_{1}}{\sigma}, \dotsc, \appS{t_{n}}{\sigma})}_{v}
      \tag*{def. substitution} \\
      & = \symInt{f} \parens*{\sem{\appS{t_{1}}{\sigma}}_{v}, \dotsc, \sem{\appS{t_{n}}{\sigma}}_{v}}
      \tag*{def. semantics} \\
      & = \symInt{f} \parens*{\sem{\appS{t_{1}}{\sigma'}}_{v'}, \dotsc, \sem{\appS{t_{n}}{\sigma'}}_{v'}}
      \tag*{induction hypothesis} \\
      & = \sem{\appS{f(t_{1}, \dotsc, t_{n})}{\sigma'}}_{v'}
      \tag*{def. semantics and subst.}
    \end{align*}
  \end{itemize}
  Thus, by induction on $t$, the sought after identity
  $\sem{\appS{t}{\sigma}}_{v} = \sem{\appS{t}{\sigma'}}_{v'}$ holds.
\end{proof}

The following lemma extends \cref{lem:substitution-to-valution-on-terms} to formulas.
\begin{lemma}{}{substitution-to-valution-on-formulas}
  For all substitutions $\sigma$, formulas $\phi$, valuations $v$ and variables $x$,
  where $x$ is fresh for $\sigma$, the following equation holds.
  \begin{equation*}
    \sem{\appS{\phi}{\sigma}}_{v}
    = \sem{\appS{\phi}{(\sigma \update{x}{x})}}_{v \updateV{x}{\sem{\sigma(x)}_{v}}}
  \end{equation*}
\end{lemma}
\begin{proof}
  As in \cref{lem:substitution-to-valution-on-terms}, we let $\sigma' = \sigma \update{x}{x}$ and
  $v' = v \updateV{x}{\sem{\sigma(x)}_{v}}$, and then prove
  \begin{equation*}
    \sem{\appS{\phi}{\sigma}}_{v} = \sem{\appS{\phi}{\sigma'}}_{v'}
  \end{equation*}
  by induction on $\phi$.
  \begin{itemize}
  \item In the predicate base case, we have
    \begin{align*}
      \sem{\appS{P(t_{1}, \dotsc, t_{n})}{\sigma}}_{v}
      & = \sem{P(\appS{t_{1}}{\sigma}, \dotsc, \appS{t_{n}}{\sigma})}_{v}
      \tag*{def. substitution} \\
      & = \symInt{P}\parens*{\sem{\appS{t_{1}}{\sigma}}_{v}, \dotsc, \sem{\appS{t_{n}}{\sigma}}_{v}}
      \tag*{def. semantics} \\
      & = \symInt{P}\parens*{\sem{\appS{t_{1}}{\sigma'}}_{v'}, \dotsc, \sem{\appS{t_{n}}{\sigma'}}_{v'}}
      \tag*{\cref{lem:substitution-to-valution-on-terms}} \\
      & = \sem{\appS{P(t_{1}, \dotsc, t_{n})}{\sigma'}}_{v'}
      \tag*{def. semantics and subst.}
    \end{align*}
  \item The base case for $\bot$ is trivial:
    $\sem{\appS{\bot}{\sigma}}_{v} = 0 = \sem{\appS{\bot}{\sigma'}}_{v'}$.
  \item The cases for conjunction, disjunction and implication are immediate by the
    induction hypothesis.
    We write $\B_{\land} = \min$, $\B_{\lor} = \max$ and $\B_{\to} (x,y)= x \Rightarrow y$,
    which are the binary Boolean functions for their corresponding connective.
    This gives us
    \begin{align*}
      \sem{\appS{(\phi_{1} \BoxOp \phi_{2})}{\sigma}}_{v}
      & = \B_{\BoxOp}(\sem{\appS{\phi_{1}}{\sigma}}_{v}, \sem{\appS{\phi_{2}}{\sigma}}_{v})
      \tag*{def. subst. and semantics} \\
      & = \B_{\BoxOp}(\sem{\appS{\phi_{1}}{\sigma'}}_{v'}, \sem{\appS{\phi_{2}}{\sigma'}}_{v'})
      \tag*{induction hyp.} \\
      & = \sem{\appS{(\phi_{1} \BoxOp \phi_{2})}{\sigma'}}_{v'}
      \tag*{def. semantics and subst.}
    \end{align*}
  \item For quantifiers assume that $y$ is fresh for $\sigma$.

    Before we continue, observe that for any $a \in U$, we can define
    $w = v \updateV{y}{a}$ and $w' = w \updateV{x}{\sem{\sigma(x)}_{v}}$.
    By using the induction hypothesis for $\psi$ with $w$, we obtain
    $\sem{\appS{\psi}{\sigma}}_{w} = \sem{\appS{\psi}{\sigma'}}_{w'}$.
    Since $x$ and $y$ are fresh, we have that
    $w' = v' \updateV{y}{a}$.
    Thus
    $\sem{\appS{\psi}{\sigma}}_{w} = \sem{\appS{\psi}{\sigma'}}_{v' \updateV{y}{a}}$.

    With this observation, we have
    \begin{align*}
      \sem{\appS{(\all{y} \psi)}{\sigma}}_{v}
      & = \sem{\all{y} \appS{\psi}{\sigma}}_{v}
      \tag{SQ} \\
      & = \min \setDef{\sem{\appS{\psi}{\sigma}}_{v \updateV{y}{a}}}{a \in U}
      \tag*{def. semantics} \\
      & = \min \setDef{\sem{\appS{\psi}{\change{\sigma'}}}_{\change{v'} \updateV{y}{a}}}{a \in U}
      \tag*{see above} \\
      & = \sem{\appS{(\all{y} \psi)}{\sigma'}}_{v'}
      \tag*{def. semantics and (SQ)}
    \end{align*}

    The same reasoning, replacing $\min$ by $\max$, gives us the result also for the existential
    quantifier.
  \end{itemize}
  This concludes the induction and thereby the proof.
\end{proof}

\begin{proof}[Proof of \cref{thm:fol-soundness}]
  We generalise the statement and prove that $\seq[\Delta]{\Gamma}{\phi}$ implies
  $\Gamma \entails \phi$.
  To this end, we proceed by induction on the proof tree for $\seq[\Delta]{\Gamma}{\phi}$
  in \cNDF{}.
  The statement for \NDF{} follows from this.

  Thus, assume that we are given a proof tree for $\seq[\Delta]{\Gamma}{\phi}$.
  We have to show for all models $\Model$ and valuations $v$ in $\Model$ that
  $\sem{\Gamma}_{v} \leq \sem{\phi}_{v}$.
  Most of the cases are dealt with in the same way as for propositional logic
  and we only treat the rules for quantifiers here.
  \begin{itemize}
  \item Suppose the proof tree ends in
    \begin{equation*}
      \begin{prooftree}
        \hypo{\seq[\Delta,x]{\Gamma}{\phi}}
        \infer1[\IAll]{\seq[\Delta]{\Gamma}{\all{x} \phi}}
      \end{prooftree}
    \end{equation*}
    where $x$ does not appear in $\Delta$.
    We now have
    \begin{align*}
      \sem{\all{x} \phi}_{v}
      & = \min \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}
      \tag*{def. semantics} \\
      & \geq \min \setDef{\sem{\Gamma}_{v \updateV{x}{a}}}{a \in \Univ}
      \tag*{by IH} \\
      & = \sem{\Gamma}_{v},
      \tag*{$x$ not in $\Gamma$}
    \end{align*}
    where we use the induction hypothesis (IH) for $\seq[\Delta,x]{\Gamma}{\phi}$
    with model $\Model$ and valuation $v \updateV{x}{a}$.
    This identity gives us $\Gamma \entails \all{x} \phi$.
  \item Next, suppose that last rule that is used in the tree is
    \begin{equation*}
      \begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\all{x} \phi}}
        \infer1[\EAll]{\seq[\Delta]{\Gamma}{\appS{\phi}{\update{x}{t}}}}
      \end{prooftree}
    \end{equation*}
    for some term $t$ with variables in $\Delta$.
    By the induction hypothesis, we have that $\sem{\Gamma}_{v} \leq \sem{\all{x} \phi}_{v}$.
    Using the substitution lemma with the substitution $\sigma = \update{x}{t}$, we obtain
    the desired result:
    \begin{align*}
      \sem{\Gamma}_{v}
      & \leq \sem{\all{x} \phi}_{v}
      \tag*{by IH} \\
      & = \min \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}
      \tag*{by def.} \\
      & \leq \sem{\phi}_{v \updateV{x}{\sem{t}_{v}}}
      \tag*{property of $\min$} \\
      & = \sem{\appS{\phi}{\update{x}{t}}}_{v}
      \tag*{\cref{lem:substitution-to-valution-on-formulas}}
    \end{align*}
  \item The proof for the introduction of existential quantifiers
    \begin{equation*}
      \begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\appS{\phi}{\update{x}{t}}}}
        \infer1[\IEx]{\seq[\Delta]{\Gamma}{\exist{x} \phi}}
      \end{prooftree}
    \end{equation*}
    follows a similar argument:
    \begin{align*}
      \sem{\Gamma}_{v}
      & \leq \sem{\appS{\phi}{\update{x}{t}}}_{v}
      \tag*{by IH} \\
      & = \sem{\phi}_{v \updateV{x}{\sem{t}_{v}}}
      \tag*{\cref{lem:substitution-to-valution-on-formulas}} \\
      & \leq \max \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}
      \tag*{property of $\max$} \\
      & = \sem{\exist{x} \phi}_{v}
      \tag*{by def.}
    \end{align*}
  \item Finally, the elimination of existential quantifiers is probably the most
    tricky part.
    Suppose that the tree ends with this rule application:
    \begin{equation*}
      \begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\exist{x} \phi}}
        \hypo{\seq[\Delta,x]{\Gamma,\phi}{\psi}}
        \infer[left label={($x \not\in \Delta$)}]2[\EEx]{\seq[\Delta]{\Gamma}{\psi}}
      \end{prooftree}
    \end{equation*}
    By the induction hypothesis, we have
    \begin{enumerate}[label=\Roman*)]
    \item
      $\sem{\Gamma}_{v} \leq \sem{\exist{x} \phi}_{v}
      = \max \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}$, and
    \item for all $a \in \Univ$
      \begin{align*}
        \min \set{ \sem{\Gamma}_{v}, \sem{\phi}_{v \updateV{x}{a}}}
        & = \sem{\Gamma, \phi}_{v \updateV{x}{a}}
        \tag*{$x \not\in \Delta$} \\
        & \leq \sem{\psi}_{v \updateV{x}{a}}
        \tag*{by IH} \\
        & = \sem{\psi}_{v}
        \tag*{$x \not\in \Delta$}
      \end{align*}
    \end{enumerate}
    Putting these together, we obtain
    \begin{align*}
      & \sem{\Gamma}_{v} \\
      & \leq \min \set{\sem{\Gamma}_{v},
        \max \setDef{\sem{\phi}_{v \updateV{x}{a}}}{a \in \Univ}}
      \tag*{by I and monotonicity of $\min$} \\
      & \leq \sem{\psi}_{v}
      \tag*{by II}
    \end{align*}
    as required.
  \end{itemize}
  This induction in proof trees shows that $\seq[\Delta]{\Gamma}{\phi}$ in \cNDF{}
  implies $\Gamma \entails \phi$.
\end{proof}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: