\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{10}{First-Order Horn Clauses and Automatic Deduction}{fol-automation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Automatic Deduction and the Cut-Rule}
\label{sec:cut-rule}

When approaching proofs of mathematical theorems, we usually decompose the problem into
intermediate results that are easier to prove on their own.
In the natural deduction systems, the approach is justified by the so-called \emph{cut rule}.
This rule allows one to prove first a formula $\phi$, often called a \emph{lemma}, then prove a
formula $\psi$ under the assumption of $\phi$, and finally the conclude that $\psi$ holds
without explicit reference to the lemma $\phi$.
The rule is formulated in the following theorem.
\begin{theorem}{Admissible Cut}{fol-cut}
  The following \notion{cut rule} is admissible in \NDF{}.
  \begin{equation*}
    {\begin{prooftree}
        \hypo{\seq{\Gamma}{\phi}}
        \hypo{\seq{\Gamma, \phi}{\psi}}
        \infer2[\Cut]{\seq{\Gamma}{\psi}}
      \end{prooftree}}
  \end{equation*}
\end{theorem}
\begin{proof}
  The cut rule is given by the following proof tree.
  \begin{equation*}
    {\begin{prooftree}
        \hypo{\seq{\Gamma}{\phi}}
        \hypo{\seq{\Gamma, \phi}{\psi}}
        \infer1[\IImpl]{\seq{\Gamma}{\phi \to \psi}}
        \infer2[\EImpl]{\seq{\Gamma}{\psi}}
      \end{prooftree}}
    \tag*{\qedhere}
  \end{equation*}
\end{proof}

The cut rule is very useful in structuring proofs, but horrendous for automatic deduction
because it requires ingenuity for inventing the intermediate lemma $\phi$ to prove $\psi$.
There are techniques for generating such lemmas but we will take another route here.

A first step to automatic deduction is to avoid the use of the cut rule and thereby the introduction
of an implication that is immediately followed by an implication elimination, as in the
proof of the cut rule.
Fortunately, it is possible to avoid such detours and we can proceed without having to make up
formulas out of thin air.
This is captured by the following theorem.
\todo{History: First proved by Gentzen~\cite{Gentzen1935:LogSchliessenI} in his ``Hauptsatz''}
\begin{theorem}{Cut Elimination}{fol-cut-elim}
  Any proof tree in \NDF{} for a sequent $\seq{\Gamma}{\phi}$ can be transformed into a proof tree
  in \NDF{} for the same sequent, which contains no instances of the cut rule.
\end{theorem}
\begin{proof}
  We shall only discuss the idea how detours introduced by a cut can be removed, as its
  implementation is rather technical.
  Details can be found in texts on
  proof theory~\cite{TroelstraDalen-ConstrMath,Troelstra00:BasicProofTheory}

  In the \Cut{}-rule, we split the proof a lemma $\phi$ from its use in a proof of $\psi$.
  To avoid a cut, we can thus use the proof of $\phi$ directly everywhere we use the assumption
  $\phi$ in the proof of $\psi$.
  More precisely, suppose we have a proof tree
  \begin{equation*}
    {\begin{prooftree}
        \hypo{\vdots}
        \infer1{\seq{\Gamma}{\phi}}
      \end{prooftree}}
  \end{equation*}
  for the sequent $\seq{\Gamma}{\phi}$.
  Any proof tree for $\seq{\Gamma,\phi}{\psi}$ can only use $\phi$ through the
  $\Ax$-rule and the tree must be of the following form, where we display only
  one use of the $\Ax$-rule.
  \begin{equation*}
    {\begin{prooftree}
        \infer0[\Ax]{\seq{\Gamma, \phi}{\phi}}
        \hypo{\dotsm}
        \infer2{\seq{\Gamma, \phi}{\delta}}
        \hypo{\vdots}
        \infer2{\seq{\Gamma, \phi}{\psi}}
      \end{prooftree}}
  \end{equation*}
  We can now replace everywhere the use of the $\Ax$-rule applied to $\phi$ directly by the proof of
  $\phi$, as in the following tree.
  \begin{equation*}
    {\begin{prooftree}
        \hypo{\vdots}
        \infer1{\seq{\Gamma}{\phi}}
        \hypo{\dotsm}
        \infer2{\seq{\Gamma}{\delta}}
        \hypo{\vdots}
        \infer2{\seq{\Gamma}{\psi}}
      \end{prooftree}}
  \end{equation*}
  Note that the assumption $\phi$ goes away.
  This procedure can be carried out more precisely by induction over proof trees, allowing us
  substitute the proof tree for $\phi$ into that of $\psi$ and thus eliminating the application
  of the \Cut{}-rule.
\end{proof}

This is good news for automatic deduction because we can limit at each step the rules that may
be applied by inspecting the formula $\phi$ that we have to prove.
The approach of inspecting $\phi$ in finding a proof for $\seq{\Gamma}{\phi}$ is also called
\notion{goal-oriented} because the search for the proof is driven by the goal $\phi$.
However, restricting ourselves to proofs that avoid the cut rule is not enough for fully automatic
deduction because there are other choice-points.
For instance, if we try to prove $\seq{\exist{x} P(x)}{\neg \all{x} \neg P(x)}$ do we first use the
introduction of the negation or the elimination of the existential quantifier?
The problem is that the principle formula of elimination rules, the formula that gives a rule its
name, appears among the premises of those rules.
This means for the goal-oriented construction of a proof that we have to guess the right formula
to eliminate from the goal, while there may be a non-trivial relation between the two.
In the above example, we have to guess that we have to eliminate $\exist{x} P(x)$ to prove
$\neg \all{x} \neg P(x)$, which is difficult for human intelligence, let alone for a computer.
One way out of this is to limit the class of formulas that may appear in proofs and thereby reduce
the amount of guessing to a manageable amount.
Combined with limiting proof rules, we obtain a reasonable fragment of first-order logic
for automatic deduction.

\section{First-Order Horn Clauses and Logic Programming}
\label{sec:fol-horn-lp}

In \cref{chap:pl-automation}, we have seen a class of propositional formulas that were called Horn clauses.
It turns out that there is an extremely useful generalisation to first-order logic.
\begin{definition}{Horn Clauses and Theories in FOL}{fol-horn}
  A \notion{predicate atom} over a signature $\Lang$ is a formula of the form $P(t_{1}, \dotsc, t_{n})$,
  where $P$ is an $n$-ary predicate symbol in $\Lang$ and $t_{1}, \dotsc, t_{n}$ are $\Lang$-terms.
  Predicate atoms are denoted by letters $A, B, C, \dotsc$
  A \notion{Horn clause} is a closed formula of the form
  \begin{equation*}
    \all{x_{1}} \dotsm \all{x_{m}} A_{1} \land \dotsm \land A_{n} \to A_{0}
  \end{equation*}
  for $m, n \in \N$ and predicate atoms $A_{0}, A_{1}, \dotsc, A_{n}$.
  The atom $A_{0}$ is called the \notion{head} of the clause and the set
  $\set{A_{1}, \dotsc, A_{n}}$ is called the \notion{body} of the clause.
  We call a list $\Gamma$ of Horn clauses a \notion{Horn clause theory} or \notion{logic program}.
\end{definition}
The body of a clause may be empty, that is, $n$ may be zero, in which case we leave out the
implication in the Horn clause in \cref{def:fol-horn} and just write
$\all{x_{1}} \dotsm \all{x_{m}} A_{0}$.
This  is convenient when writing logic programs that contain Horn clauses without assumptions,
so called \notion{base facts}.
In the abstract treatment of Horn clauses later in this chapter it will, however, not be necessary
to differentiate between base facts and general Horn clauses.

It is also important to note that a Horn clause must be closed, thus only the variables
$x_{1}, \dotsc, x_{m}$ may appear in the head and body of a Horn clause.
These are then all bound by the quantifiers on the outside.

What makes Horn clause theories so useful?
As the name ``logic program'' indicates, we can use such theories for programming.
In fact, we can describe \emph{any} Turing machine by a logic program $\Gamma$.
A computation corresponds to giving a predicate atom $A$ with free variables $x_{1}, \dotsc, x_{m}$
and asking for a derivation of $\seq{\Gamma}{\exist{x_{1}} \dotsm \exist{x_{m}} A}$ in \NDF{}.
If the Turing machine halts, then such a derivation exists.
This correspondence makes it possible to automatically find such a derivation if it exists.
Thus, finding proofs is also for Horn clause theories only semi-decidable but, as we will see,
it is still much easier in many cases to find proofs from Horn clause theories rather than
arbitrary theories.

Let us demonstrate the programming aspect of Horn clauses.
Recall that in primitive recursive arithmetic and other approaches to arithmetic, we have always
assumed that all objects were numbers.
Often, we need to reason about different \emph{types} of objects.
The following example shows how we can axiomatise with Horn clauses a predicate that characterises
natural numbers.
\begin{example}{}{horn-clause-arithmetic}
  In this example, we will use that every natural number is either zero or the successor of another
  natural number.
  More specifically, the constant $\zero$ represents in the following zero and the unary function symbol
  $s$ represents the successor of a number.
  With these two symbols, we can enumerate all the natural numbers:
  \begin{equation*}
    \zero, \, s(\zero), \, s(s(\zero)), \, s(s(s(\zero))), \dotsc
  \end{equation*}
  Writing natural numbers in this way is called \notion{unary encoding}, which is a very bad
  encoding from the perspective of complexity but very useful for explanatory purposes.

  In this example, we will be working with a signature $\Lang$ that contains a constant $\zero$,
  a unary function symbol $s$, a unary predicate symbol $N$, a binary predicate symbol $L$,
  and a ternary predicate symbol $P$.
  We use the unary predicate symbol $N$ to pin down the natural numbers, in the sense
  that $N(t)$ holds if $t$ is the representation of a natural number.
  The following two Horn clauses implement precisely the initial description of natural numbers.
  \begin{alignat*}{2}
    &                     & & N(\zero) \\
    & \all{n} N(n) \to \; & & N(s(n))
  \end{alignat*}

  We can now continue and implement arithmetic for natural numbers.
  To use Horn clauses for this task, we have to switch from thinking in terms of function to
  thinking in terms of relations.
  More specifically, we use a ternary predicate symbol $P$ with the intent that $P(m, n, k)$ holds
  if $k$ is the sum of $m$ and $n$.
  To provide Horn clauses that implement addition, recall the primitive recursive specification of
  addition: $0 + m = m$ and $(n + 1) + m = (n + m) + 1$.
  Assuming that $n$ and $m$ are given in the above representation of natural numbers, we can
  represent this recursive specification of addition by the following two Horn clauses.
  \begin{alignat*}{2}
    & \all{m} & & P(\zero, m, m) \\
    & \all{n} \all{m} \all{k} P(n, m, k) \to \; & & P(s(n), m, s(k))
  \end{alignat*}

  % How does this relate to our previous description of natural numbers in terms of the binary
  % function symbol $p$ for plus?
  % We can use Horn clauses to describe how $p$ computes!
  % To achieve this, we will use a binary predicate symbol $E$ for \textbf{E}valuating arithmetic
  % expressions to get something of the shape described by the predicate $N$.
  % % \begin{alignat*}{2}
  % %   &                         &     & E(\zero, \zero) \\
  % %   & \all{n} \all{k} E(n, k) & \to & E(s(n), s(k)) \\
  % %   &                         &     & E(\sym{0}, \zero) \\
  % %   & \all{k} E(\sym{0}, k)   & \to & E(\sym{1}, s(k)) \\
  % %   & \all{k} E(\sym{1}, k)   & \to & E(\sym{2}, s(k)) \\
  % %   & \all{m_{0}} \all{n_{0}} \all{m} \all{n} \all{k} E(m_{0}, m) \land E(n_{0}, n) \land P(m, n, k) & \to & E(p(m_{0}, n_{0}), k)
  % % \end{alignat*}
  % \begin{align}
  %   & E(\zero, \zero)
  %   \label{eq:eval-z} \\
  %   & \all{n} \all{k} E(n, k) \to E(s(n), s(k))
  %   \label{eq:eval-s} \\
  %   % &            &             &     & E(\sym{0}, \zero) \\
  %   % & \all{k} & E(\sym{0}, k)   & \to & E(\sym{1}, s(k)) \\
  %   % & \all{k} & E(\sym{1}, k)   & \to & E(\sym{2}, s(k)) \\
  %   &
  %   \begin{aligned}[b]
  %     \all{m_{0}} & \all{n_{0}} \all{m} \all{n} \all{k} \\
  %     & E(m_{0}, m) \land E(n_{0}, n) \land P(m, n, k) \to E(p(m_{0}, n_{0}), k)
  %   \end{aligned}
  %   \label{eq:eval-p}
  % \end{align}
  % The idea of these three Horn clauses is that \cref{eq:eval-z} evaluates $\zero$ to $\zero$,
  % \cref{eq:eval-s} allows evaluation under the successor symbol,
  % and \cref{eq:eval-p} evaluates the symbol $p$ by summing up the arguments of $p$.
  % Intuitively, to show that $E(p(\zero, s(\zero)), s(\zero))$ holds, we would have to show by \cref{eq:eval-p}
  % that $E(\zero, \zero)$, $E(s(\zero), s(\zero))$ and $P(\zero, s(\zero), s(\zero))$ holds.

  In a similar spirit, we can define other recursive relations on natural numbers.
  For instance, to specify that the binary predicate $L$ represents the less or equal relation, we
  would use the following two Horn clauses.
  \begin{alignat*}{2}
    & \all{n} && L(n, n) \\
    & \all{n} \all{m} L(n, m) & \; \to \; & L(n, s(m))
  \end{alignat*}
  The clauses say that every natural number is less or equal to itself (reflexivity) and that
  increasing the number on the right preserves the less or equal relation.
\end{example}

\begin{quiz}
  Can you find two Horn clauses that describe the computation of multiplication as ternary predicate
  symbol $M$ analogously to $P$ in \cref{ex:horn-clause-arithmetic}?
\end{quiz}
\begin{answer}
  The idea is that multiplication is given recursively by
  $0 \cdot n = 0$ and $(m + 1) \cdot n = (m \cdot n) + n$.
  This can be translated into the following two Horn clauses.
  \begin{alignat*}{2}
    & \all{n} & & M(0, n, 0) \\
    & \all{m} \all{n} \all{i} \all{k} M(m, n, i) \land P(i, n, k) & \; \to \; & M(s(m), n, k)
  \end{alignat*}
\end{answer}

What makes logic programming so interesting compared to other programming paradigms?
The answer lies in its declarative nature, which allows us to specify what the result of a
computation should be rather than how the result is computed.
In his seminal work, \textcite{Kowalski79:AlgorithmLogicControl} has expressed this in the
following equation.
\begin{defbox}
  \begin{center}
    Algorithm = Logic + Control
  \end{center}
\end{defbox}
This equation essentially means that we can design algorithms by providing logical formulas
that describe the result of an algorithm and a control mechanism that controls the use of these
formulas to compute the result.
A particular property of this approach is that new algorithms can be obtained by exchanging the
control mechanism, while preserving the logical properties and correctness of the computed result.
In terms of the above equation, we may have a logical specification $L$ and algorithms $A_{1}$ and
$A_{2}$ given by $A_{1} = L + C_{1}$ and $A_{2} = L + C_{2}$ for some control mechanisms $C_{1}$ and
$C_{2}$.
These two algorithms will compute results with the same logical properties, but may have
different computational behaviour.
For example, $A_{2}$ may be more efficient than $A_{1}$.

Let us return to concrete logic programming approach and illustrate it on our initial robot example.
\begin{example}{Path Finding}{lp-robot}
  Remember the board from \cref{fig:robot-field} on \cpageref{fig:robot-field} that Isaac drew?
  In \cref{sec:fol-motivation}, we used first-order logic to describe what moves on the board are
  allowed and how a route towards the heart looks like.
  The aim of this example is to use Horn clauses to give a logical description of paths that will
  allow us, with an appropriate control mechanism, to derive an efficient algorithm for routing.

  In \cref{sec:fol-expressiveness}, we have seen that reachability in graphs cannot be expressed by
  a first-order formula.
  Since path finding for the robot can be seen as reachability in a graph, we cannot directly
  express paths as a formula.
  In exercise 3 of \cref{chap:fol-limits}, we saw how graph reachability could still be expressed
  by introducing a new predicate that represented the reflexive, transitive closure of the edge
  relation of a graph.
  And even better, the two formulas that expressed this are Horn clauses and we can therefore
  formulate graph reachability as a logic program!
  Let us now use this approach to help Isaac break this limitation of first-order logic and to
  finally get to the heart.

  For this purpose, we will use the signature $\Lang$ with constants
  $\sym{1}, \sym{2}, \dotsc$ for all positive natural numbers,
  one binary function symbol $\posF$,
  three unary predicate symbols $R$, $H$ and $F$, and
  two binary predicate symbols $A$ and $C$.
  Let us explain the intention of all these symbols.
  We will use $\posF$ to describe positions on the board and will denote by $\posF(x,y)$ the
  position with horizontal coordinate $x$ and vertical coordinate $y$.
  For instance, the robot is in \cref{fig:robot-field} in position $\mkpos{2}{3}$.
  The unary predicate symbols $R$, $H$ and $F$ describe, respectively, the position of the
  \textbf{R}obot, the position of the \textbf{H}eart and \textbf{F}ree positions.
  Since the robot may only move between adjacent positions, will use the binary predicate symbol
  $A$ to express when a position is \textbf{A}djacent to another position.
  Finally, the binary predicate symbol $C$ is what we are after: it will relate two positions
  if they are \textbf{C}onnected by a path via free and adjacent positions.

  Our goal is to describe the situation in \cref{fig:robot-field} and the predicate symbol $C$
  as a logic program, that is, a finite list of Horn clauses.
  All of the following is provided in \cref{chap:logic-programming} as a Prolog program that
  can be directly run in your favourite Prolog interpreter.

  Let us begin with the easy part: the position of the robot and the heart.
  These are given by the following two formulas.
  \begin{equation}
    \label{eq:robot-heart-position}
    R(\mkpos{2}{3})
    \quad \text{and} \quad
    H(\mkpos{5}{1})
  \end{equation}
  This two formulas are base facts, albeit with no quantifiers and an empty body, that is,
  $m$ and $n$ in \cref{def:fol-horn} are both zero.

  Next, we describe the free position that the robot may visit by providing a formula for
  each free position:
  \begin{equation}
    \label{eq:free-positions}
    \begin{aligned}
      & F(\mkpos{1}{1})
      & & F(\mkpos{2}{2})
      & & F(\mkpos{3}{1})
      \\
      & F(\mkpos{1}{4})
      & & F(\mkpos{2}{3})
      & & F(\mkpos{3}{2})
      & \dots
      \\
      &
      & & F(\mkpos{2}{4})
      & & F(\mkpos{3}{4})
    \end{aligned}
  \end{equation}
  You may have noticed that we have initially talked in \cref{sec:fol-motivation} about the
  obstacles on the board and now we talk about free positions instead.
  The reason for this is that obstacles are an inherently negative description of the allowed
  moves that the robot may make.
  More precisely, we have the relation
  \begin{equation*}
    \all{p} F(p) \bimpl \neg O(p) \, ,
  \end{equation*}
  where $O$ was the predicate symbol that we used for describing the positions of obstacles.
  This negative relation between $F$ and $O$ would become a problem when we ask if the robot
  can move to a certain position because the first-order Horn clauses in \cref{def:fol-horn}
  may not use $\bot$ and therefore no negation!
  Thus, we would not be able to formulate the routing problem as a logic program if we describe
  positions with obstacles instead of free positions.

  To finish the board description, we have to give all the adjacent positions by providing
  formulas that specify the predicate symbol $A$.
  This predicate should be read as, if $A(p,q)$ holds then position $p$ and $q$ are adjacent.
  For instance, we should have that $A(\mkpos{1}{1}, \mkpos{1}{2})$ holds.
  However, neither $A(\mkpos{1}{1}, \mkpos{2}{2})$ nor $A(\mkpos{6}{1}, \mkpos{7}{1})$ should not
  hold.
  In the first case, the two positions $\mkpos{1}{1}$ and $\mkpos{2}{2}$ are not adjacent,
  as we do not allow diagonal steps.
  In the second case, the term $\mkpos{7}{1}$ is not a valid position on the board because the
  horizontal coordinates range from $1$ to $6$ only.
  Formalising all of this as Horn clauses is a bit tedious because we have to enumerate for each
  position all its four neighbours, except for positions at the boundary that have two or three
  neighbours:
  \begin{equation}
    \label{eq:adjacent-positions}
    \begin{aligned}
      & A(\mkpos{x}{y}, \mkpos{x + 1}{y}), & 1 \leq x \leq 5, & & 1 \leq y \leq 4 \\
      & A(\mkpos{x}{y}, \mkpos{x}{y+1}),   & 1 \leq x \leq 6, & & 1 \leq y \leq 3 \\
      & A(\mkpos{x}{y}, \mkpos{x - 1}{y}), & 2 \leq x \leq 6, & & 1 \leq y \leq 4 \\
      & A(\mkpos{x}{y}, \mkpos{x}{y - 1}), & 1 \leq x \leq 6, & & 2 \leq y \leq 4
    \end{aligned}
  \end{equation}
  The first line of \eqref{eq:adjacent-positions} states that every position $\mkpos{x}{y}$ is
  adjacent to its east neighbour $\mkpos{x+1}{y}$.
  Note that $\mkpos{x}{y}$ only has a neighbour in the east if it is not a boundary position,
  that is, if $x \leq 5$.
  In the remaining three lines, we analogously provide the specification of the southern, western
  and northern neighbours.

  With the board layout described through the Horn clauses in
  \cref{eq:robot-heart-position,eq:free-positions,eq:adjacent-positions}, we can now try to find
  a way for Isaac to the heart.
  We achieve this by providing a description of the predicate symbol $C$ with the intention
  that $C(p,q)$ holds if position $q$ is reachable from $p$ via free and adjacent positions.
  Reachability can be described by two Horn clauses, one that states that every position
  can be reached from itself and one for making an intermediate step via a free position:
  \begin{alignat}{2}
    & \all{p} & C(p,p)
    \label{eq:reachability-refl}
    \\
    & \all{p} \all{q} \all{r} C(q,r) \land A(p,q) \land F(q) \to \; & C(p,r)
    \label{eq:reachability-trans}
  \end{alignat}

  These two Horn clauses formulate reachability as backward search.
  That is to say, we start with the final position we want to reach and then try to find
  positions that make a path to the initial position.
\end{example}


\todo[inline]{
  Start with graph reachability.

  This fits with compactness and \cref{ex:fol-expressiveness} because we introduce a new
  \emph{predicate} that expresses reachability through a recursive axiomatisation and are not
  trying to write down a \emph{formula} that would express reachability only in terms of the
  edge relation.
%  Moreover, we know that this new predicate only expresses reachability because
}
\section{Uniform Proofs}
\label{sec:uniform-proofs}

In \cref{ex:lp-robot}, we have used logical formulas to describe how a path
from the starting position to the goal looks like.
What is missing to get an algorithm for routing is the control part of Kowalski's
equation, which allows us to search for a path.
We will introduce in this section a control mechanism based on proof theory, called uniform proofs.

The idea of uniform proofs is that we devise a proof system tailored towards proving goal
formulas of a very specific shape only from Horn clauses.
This restriction of goals and assumptions has a two-fold effect:\todo{Mention that all
  elimination rules removed or concentrated in one rule}
\begin{enumerate}
\item we remove all the choices appearing in elimination rules, and
\item we concentrate the choices in one proof rule.
\end{enumerate}
Removing elimination rules in general and the cut rule in particular, rules out an infinitude of
choices and makes the search for proof rules more goal-oriented.
As we will see, the second aspect of concentrating choice, reduces the amount of branching
in proof search drastically to just a few options.
All together, uniform proofs provide an operational perspective on proof search and will guide
us in the construction of proofs, also in \NDF{}, relative to logic programs.

Before we come to the actual definition of uniform proofs, we will need to talk about specific
terms and substitutions that arise in proof search.
Recall that a term with no variables is called closed.
In the simplest case, which will be the only one that we treat here, we can prove an existentially
quantified formula $\exist{x} \phi$ by providing a closed term $t$ and prove $\phi \update{x}{t}$.
For instance, let $\phi$ be $R(\posF(x, \sym{3}))$ and let $t$ be $\sym{2}$, as in
\cref{ex:lp-robot}.
Under the assumption of \cref{eq:robot-heart-position}, we have that
$\phi \update{x}{t}$ holds and thus also $\exist{x} (\posF(x, \sym{3}))$.
Note that we substituted the closed term $\sym{2}$ for $x$ and thereby obtained a closed formula.%
\footnote{Terminology: Closed terms and formulas are often also called ground terms and formulas.}
We will restrict the proof rules \IEx{} and \EAll{} of \NDF{} to allow only substitutions that
result into closed formulas, which in turn will eliminate all free variables from proofs.
The following definition formalises the needed substitutions.

\begin{definition}{Closing substitution}{closing-subst}
  Let $X \subseteq \Var$ be a set of variables.
  An $X$-\notion{closing substitution} is a substitution
  $\sigma \from \Var \to \Term$, such that $\sigma(x)$ is closed for all $x \in X$.
\end{definition}

The purpose of $X$-closing substitutions is to turn any formula, whose free variables are among
those in $X$, into a closed formula.
\begin{example}{}{}
  As we have $\fv(P(\sym{0}, x, x)) = \set{x}$, any $X$-closing substitution with $x \in X$
  will allow use to close this formula.
  For instance, $s(\sym{0})$ is a closed term over the signature of \cref{ex:horn-clause-arithmetic}
  and $\update{x}{s(\sym{0})}$ is a $\set{x}$-closing substitution.
  Indeed, we obtain the closed formula
  $\appS{P(\sym{0}, x, x)}{\update{x}{s(\sym{0})}} = P(\sym{0}, s(\sym{0}), s(\sym{0}))$.
\end{example}

\begin{quiz}
  Do signatures without constants admit closed terms?
\end{quiz}
\begin{answer}
  No, if a signature $\Lang$ has no constants, then it is not possible to obtain an
  $\Lang$-term without variables.
  For example, suppose $\Lang$ is a signature with only one unary function symbol $f$.
  Then the only terms we can build over this signature are of the form $x, f(x), f(f(x)), \dotsc$
  for variables $x$.
  Thus, it is not possible to obtain a closed term without variables from $\Lang$.
  This can formally be proven by induction over first-order terms.
\end{answer}

We mentioned initially that we obtain proof search by restricting the formulas $\phi$ that
can appear as a goal, that is, in a sequent $\seq{\Gamma}{\phi}$ that we wish to prove.
Out of the many choices one can make to restrict formulas, we will be using the one
given in \cref{def:goal-formulas} below.
Before we come to that definition, let us briefly think about what a reasonable set of
goals, for which proofs can be searched, would be.
First of all, we of course want to be able to prove facts about predicates, hence predicate
atoms of the form $P(t_{1}, \dotsc, t_{n})$ should certainly be allowed as goals.
Next, conjunction does certainly not pose any problem because finding a proof for
$\seq{\Gamma}{\phi \land \psi}$ only requires us to find proofs for $\seq{\Gamma}{\phi}$
and $\seq{\Gamma}{\psi}$ separately by the introduction rule \IAnd{}.
To be able to state and prove more interesting properties, we will also allow disjunction
and existential quantification.
These two connectives will make proof search more difficult.
Recall that the introduction rules \LIOr{} and \RIOr{} for disjunction require us to find proofs
for \emph{either} $\seq{\Gamma}{\phi}$ \emph{or} $\seq{\Gamma}{\psi}$ in order to prove
$\seq{\Gamma}{\phi \lor \psi}$.
Thus, to find a proof for the disjunction $\phi \lor \psi$ we have to try to prove one of the
options, say $\phi$, and if we fail to prove this option, then we try the other.
Trying out these different options is not difficult, but can be costly, depending on how
many proof steps we have to carry out before we find out that $\phi$ is not provable.
Finally, the proof of an existential quantifier $\exist{x} \phi$ requires us to find a term $t$,
such that $\phi \update{x}{t}$ holds.
As it turns out, it is possible to devise procedure that construct a closed term $t$, if it exists,
automatically.\todo{Section on unification}
% and we see how this can be done in \cref{sec:unification}.
Thus, we will also allow existential quantifiers in proof goals.
The following \cref{def:goal-formulas} sums up the formulas that we allow as goals.

\begin{definition}{Goal formulas}{goal-formulas}
  The set $\Goal$ of all \notion{goal formulas} $\phi_{G}$ is the set of $\Lang$-formulas over a
  signature $\Lang$ generated by the following grammar.
  \begin{equation*}
    \phi_{G}
    \cce P(t_{1}, \dotsc, t_{n})
    \mid \phi_{G} \land \phi_{G}
    \mid \phi_{G} \lor \phi_{G}
    \mid \exist{x} \phi_{G}
  \end{equation*}
\end{definition}

\begin{figure}[ht]
  \centering
  \begin{defbox}[title={Introduction Rules}]
    \begin{gather*}
      {\begin{prooftree}
          \hypo{\ufseq{\phi_{1}}}
          \hypo{\ufseq{\phi_{2}}}
          \infer2[\IAnd]{\ufseq{\phi_{1} \land \phi_{2}}}
        \end{prooftree}}
      \qquad
      {\begin{prooftree}
          \hypo{\ufseq{\phi \update{x}{t}}}
          \hypo{t \text{ closed}}
          \infer2[\IEx]{\ufseq{\exist{x} \phi}}
        \end{prooftree}}
      \\[7pt]
      {\begin{prooftree}
          \hypo{\ufseq{\phi_{1}}}
          \infer1[\LIOr]{\ufseq{\phi_{1} \lor \phi_{2}}}
        \end{prooftree}}
      \qquad
      {\begin{prooftree}
          \hypo{\ufseq{\phi_{2}}}
          \infer1[\RIOr]{\ufseq{\phi_{1} \lor \phi_{2}}}
        \end{prooftree}}
    \end{gather*}
  \end{defbox}
  \begin{defbox}[title={Backchaining Rule}]
    \begin{equation*}
      {\begin{prooftree}
          \hypo{
            \parens*{\all{x_{1}} \dotsm \all{x_{m}} A_{1} \land \dotsm A_{n} \to A_{0}} : \Gamma}
          \hypo{\ufseq{A_{1}[\sigma]} \; \dotsm \; \ufseq{A_{n}[\sigma]}}
          \infer2[\BackCh]{\ufseq{A_{0} [\sigma]}}
        \end{prooftree}}
    \end{equation*}
    where $\sigma$ is an $\set{x_{1}, \dotsc, x_{m}}$-closing substitution.
  \end{defbox}
  \caption{Rules of Uniform Proofs}
  \label{fig:rules-uf}
\end{figure}

Let us now come to uniform proofs, which are given by a proof system that allows us to prove
goals $\phi$ (\cref{def:goal-formulas}) from Horn clause theories $\Gamma$ (\cref{def:fol-horn}).
To distinguish sequents for uniform proofs from those of \NDF{}, we will write
$\ufseq{\phi}$ for a sequent that we intent to find a uniform proof for.
We have already explained the general approach to prove $\ufseq{\phi}$ above for the case of
conjunction, disjunction and existential quantification.
This leads us to the introduction rules in the upper part of \cref{fig:rules-uf}, which
are exactly the same rules as in \NDF{}, just restricted to the introduction of goal formulas.
Let us consider the goal $\phi$ given by $\exist{u} \exist{v} R(u) \land A(u, v) \land F(v)$ over
the signature from the robot path finding \cref{ex:lp-robot}.
This formula says that the robot is in some position on the board that has a free adjacent position.
Such positions exist, namely if we pick $p = \mkpos{2}{3}$ for the robot position and the
adjacent position $q = \mkpos{2}{4}$.
Using only the introduction rules from \cref{fig:rules-uf}, we can start a proof for $\phi$:

\begin{equation*}
  \begin{nd}
    \have {} {\vdots}
    \have {gA} {A(p, q)}                                           \by{??}{}
    \have {gF} {F(q)}                                              \by{??}{}
    \have {g3} {A(p, q) \land F(q)}                                \ai{gA,gF}
    \have {gR} {R(p)}                                              \by{??}{}
    \have {g2} {R(p) \land A(p, q) \land F(q)}                     \ai{gR,g3}
    \have {g1} {\exist{v} R(p) \land A(p, v) \land F(v)}           \Ei{g2}
    \have {g0} {\exist{u} \exist{v} R(u) \land A(u, v) \land F(v)} \Ei{g1}
  \end{nd}
\end{equation*}

However, we are not able to fill in the question marks, yet.
Note that the formulas that we have to prove in lines 2, 3 and 5 are closed predicate
atoms.
It is easy to see that any proof attempt that starts with a goal formula and only uses the
introduction rules, must eventually reach predicate atoms.
Thus, we need a rule to prove such predicate atoms from a Horn clause theory.
And that is the heart of logic programming: given a predicate atom, find a Horn clause that
matches the atom, and continue with the premises of the Horn clause.
This process is called \notion{backchaining} and is captured by the rule \BackCh{} in
\cref{fig:rules-uf}.
The idea of the backchaining rule is that, whenever we have to prove a predicate atom,
we choose a Horn clause from our logic program in such a way that the head of the clause
(\cref{def:fol-horn}) matches the atom.
If we find such a matching clause, then we continue by proving all the premises of the chosen
Horn clause.
Note that the premises of a Horn clause are always predicate atoms, thus we will continue
using the backchaining rule until we can finish the proof by selecting a fact, which is a Horn
clause without premises.

Let us use this procedure to fill in the question marks in the proof above.
For the purpose of this, let us denote by $\Gamma_{1}$ the logic program that consists of
all the Horn clauses in \cref{eq:robot-heart-position,eq:free-positions,eq:adjacent-positions}.
In line 5, we have to prove $R(\mkpos{2}{3})$, which is exactly the fact that we assumed
in \eqref{eq:robot-heart-position}.
Thus, we can use the following instance of the backchaining rule, where both $n$ and $m$ are $0$
and thus the substitution $\sigma$ is irrelevant.
\begin{equation*}
  {\begin{prooftree}
      \hypo{R(\mkpos{2}{3}) : \Gamma_{1}}
      \infer1[\BackCh]{\ufseq[\Gamma_{1}]{R(\mkpos{2}{3})}}
    \end{prooftree}}
\end{equation*}
The proofs for lines 2 and 3 are given analogously, which then results in the following completed
uniform proof in Fitch-style.

\begin{equation*}
  \begin{nd}
    \hypo {lp} {\Gamma_{1}}
    \have {gA} {A(p, q)}                                             \backch{lp}
    \have {gF} {F(q)}                                                \backch{lp}
    \have {g3} {A(p, q) \land F(q)}                                  \ai{gA,gF}
    \have {gR} {R(p)}                                                \backch{lp}
    \have {g2} {R(p) \land (A(p, q) \land F(q))}                     \ai{gR,g3}
    \have {g1} {\exist{v} R(p) \land (A(p, v) \land F(v))}           \Ei{g2}
    \have {g0} {\exist{u} \exist{v} R(u) \land (A(u, v) \land F(v))} \Ei{g1}
  \end{nd}
\end{equation*}

Before we continue to explain the backchaining rule for more interesting cases,
let us formally define what we mean by a uniform proof.
\begin{definition}{Uniform proofs}{uniform-proofs}
  Let $\Gamma$ be a Horn clause theory and $\phi$ a closed goal formula.
  We say that $\phi$ can be proven from $\Gamma$ by a \notion{uniform proof},
  if a proof for $\ufseq{\phi}$ can be derived from the rules in \cref{fig:rules-uf}.
\end{definition}

Note that the uniform proofs have no separate elimination rules.
Instead, backchaining combines the assumption rule \Ax{}, the universal
quantifier elimination \EAll{} and the implication elimination \EImpl{} into one rule.
This works because the only assumptions that we can use are Horn clauses.

% Let us try uniform proofs for a case, in which we use the backchaining
% rule on Horn clauses with assumptions and universal quantifiers.
\begin{example}{}{lp-path-finding}
  Suppose we want to show that the robot can reach \emph{some} position in \cref{fig:robot-field},
  thus we want to find a proof for
  \begin{equation*}
    \ufseq{\exist{u} C(\mkpos{2}{3}, u)} \, ,
  \end{equation*}
  where $C$ is the predicate for connected positions from \cref{ex:lp-robot}
  and $\Gamma$ the logic program that consists of
  \cref{eq:robot-heart-position,eq:free-positions,eq:adjacent-positions,%
    eq:reachability-refl,eq:reachability-trans}.
  As above, we will refer to the logic program given by
  \cref{eq:robot-heart-position,eq:free-positions,eq:adjacent-positions}
  as $\Gamma_{1}$.
  This allows us refer explicitly to the Horn clauses in
  \cref{eq:reachability-refl,eq:reachability-trans}.

  There are many choices for the position $u$, but to make
  the example non-trivial and not too lengthy, let us use $\mkpos{2}{4}$.
  The uniform proof for the above sequent goes for this choice as follows.
  \begin{equation*}
    \begin{nd}
      \hypo {lp} {\Gamma_{1}}
      \hypo {cRefl} {\all{p} C(p,p)}
      \hypo {cTrans} {\all{p} \all{q} \all{r} C(q,r) \land A(p,q) \land F(q) \to C(p,r)}
      \have {c1} {C(\mkpos{2}{4},\mkpos{2}{4})}                        \backch{cRefl}
      \have {a1} {A(\mkpos{2}{3},\mkpos{2}{4})}                        \backch{lp}
      \have {f1} {F(\mkpos{2}{4})}                                     \backch{lp}
      \have {g1} {C(\mkpos{2}{3}, \mkpos{2}{4})}                       \backch{cTrans,c1,a1,f1}
      \have {g0} {\exist{u} C(\mkpos{2}{3}, u)}                        \Ei{g1}
    \end{nd}
  \end{equation*}
\end{example}

You may notice that there is another choice hidden in \cref{ex:lp-path-finding}:
To apply the backchaining rule in line 7, we had to choose an ``intermediate'' position, the
variable $q$ in the transitivity rule from line 3.
We chose the position $\mkpos{2}{4}$ because it brought us directly to the position that we
wanted to go to.
However, we could have chosen to take a completely different path, for instance
via $\mkpos{2}{2}, \mkpos{3}{2}, \mkpos{4}{2}, \mkpos{4}{3}, \mkpos{4}{4}$ and $\mkpos{3}{4}$.
This is where we have to find an appropriate control mechanism to guide the search
for a path.
In \cref{chap:logic-programming}, an implementation of \cref{ex:lp-robot} is given in a
language called Prolog, a programming language based on Horn clause theories.
The control mechanism chosen there is ``tabling'' combined with the search for a shortest
path, which gives a very efficient search strategy for path finding.
We will not explain the details of this control mechanism or Prolog here.
For our purposes, it suffices to say that uniform proofs provide a foundation for the
reasoning steps that Prolog takes and that an algorithm can be obtained from the uniform
proof system by complementing it with an appropriate search strategy for the substitution
$\sigma$ in the backchaining rule.

\begin{remark}
  The uniform proof system has quite a few less rules than \NDF{}: all the elimination rules and
  the introduction rules for implication and universal quantification are not present.
  We have already explained why the general introduction and elimination rules for implication
  cause troubles.
  It is possible to add restricted versions of the introduction rules for implication and
  universal quantification~\cite{Miller91:UniformProofs,Miller12:ProgrammingHigherOrderLogic}.
  However, the general elimination rules need to be treated completely differently.
\end{remark}

\todo[inline]{Add some example on arithmetic}


\todo[inline]{Section on unification}
% \section{Unification*}
% \label{sec:unification}

% This section will not be relevant for the exam.

\finalisechapter{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: