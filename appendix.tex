\documentclass[logic-rondo]{subfiles}
\begin{document}

\chapter{Greek Letters}
\label{chap:greek}

\begin{table}[h]
  \centering
  \begin{tabular}{l|l|l}
    Letter & Name & LaTeX command \\\hline
    $\alpha$ $\Alpha$ & alpha & \latexCmd{alpha} \latexCmd{Alpha} \\
    $\beta$ $\Beta$ & beta  & \latexCmd{beta} \latexCmd{Beta} \\
    $\gamma$ $\Gamma$ & gamma  & \latexCmd{gamma} \latexCmd{Gamma} \\
    $\delta$ $\Delta$ & delta  & \latexCmd{delta} \latexCmd{Delta} \\
    $\epsilon$ $\Epsilon$ & epsilon  & \latexCmd{epsilon} \latexCmd{Epsilon} \\
    $\zeta$ $\Zeta$ & zeta &  \latexCmd{zeta} \latexCmd{Zeta} \\
    $\theta$ $\Theta$ & theta &  \latexCmd{theta} \latexCmd{Theta} \\
    $\iota$ $\Iota$ & iota &  \latexCmd{iota} \latexCmd{Iota} \\
    $\kappa$ $\Kappa$ & kappa &  \latexCmd{kappa} \latexCmd{Kappa} \\
    $\lambda$ $\Lambda$ & lambda &  \latexCmd{lambda} \latexCmd{Lambda} \\
    $\mu$ $\Mu$ & mu &  \latexCmd{mu} \latexCmd{Mu} \\
    $\nu$ $\Nu$ & nu &  \latexCmd{nu} \latexCmd{Nu} \\
    $\xi$ $\Xi$ & xi &  \latexCmd{xi} \latexCmd{Xi} \\
    $\omicron$ $\Omicron$ & omicron &  \latexCmd{omicron} \latexCmd{Omicron} \\
    $\pi$ $\Pi$ & pi &  \latexCmd{pi} \latexCmd{Pi} \\
    $\rho$ $\Rho$ & rho &  \latexCmd{rho} \latexCmd{Rho} \\
    $\sigma$ $\Sigma$ & sigma &  \latexCmd{sigma} \latexCmd{Sigma} \\
    $\tau$ $\Tau$ & tau &  \latexCmd{tau} \latexCmd{Tau} \\
    $\upsilon$ $\Upsilon$ & upsilon &  \latexCmd{upsilon} \latexCmd{Upsilon} \\
    $\phi$ $\Phi$ & phi &  \latexCmd{varphi} \latexCmd{Phi} \\
    $\chi$ $\Chi$ & chi &  \latexCmd{chi} \latexCmd{Chi} \\
    $\psi$ $\Psi$ & psi &  \latexCmd{psi} \latexCmd{Psi} \\
    $\omega$ $\Omega$ & omega &  \latexCmd{omega} \latexCmd{Omega}
  \end{tabular}
  \caption{Greek Letters and Their Pronunciation}
  \label{tab:greek-letters}
\end{table}

\chapter{Tools}
\label{chap:tools}

\section{Sets and Maps}
\label{sec:sets-maps}

Given sets $A$ and $B$, we denote by $A \times B$ their \termdef{product} consisting of
ordered pairs
\begin{equation*}
  A \times B = \setDef{(a,b)}{a \in A, b \in B}
\end{equation*}
and \inlinedefbox{$\Map{A}{B}$} the \termdef{set of maps} $A \to B$.
Given two maps $f \from A \to B$ and $g \from B \to C$, we denote by \inlinedefbox{$g \comp f$} the
\termdef{composition} of $g$ and $f$ with $(g \comp f)(x) = g(f(x))$.
The composition can be pronounced as ``$g$ composed with $f$'' or ``$g$ after $f$''.

We will also be using the \termdef{powerset} of sets $A$, denoted by $\Pow(A)$, which
is the set of all subsets of $A$:
\begin{equation*}
  \Pow(A) = \setDef{U}{U \subseteq A}
\end{equation*}
The \termdef{emptyset} is denoted by $\emptyset$.

The set of natural numbers starting at $0$ is denoted by $\N$.
Given a natural number $n \in \N$, we write $\fOrd{n}$ for the
\termdisp{finite ordinal}{set of the first $n$ natural numbers}:\footnote{Warning:
  In some contexts, $\fOrd{n}$ includes also $n$ and has thus $n+1$ elements. For
  our purposes, the present interpretation is the most useful though.}
\begin{equation*}
  \fOrd{n} = \setDef{k \in \N}{k < n} \, .
\end{equation*}
In particular, we have $\fOrd{0} = \emptyset$, $\fOrd{1} = \set{0}$ etc.
Generally, $\fOrd{n}$ has exactly $n$ elements.

\begin{exercise}
  \label{exerc:pairs-maps}
  Show for any set $A$ that the sets $\Map{\fOrd{2}}{A}$ and $A \times A$ are isomorphic, that is,
  there is a map $\Map{\fOrd{2}}{A} \to A \times A$ with an inverse.
\end{exercise}

\begin{exercise}
  \label{exerc:empty-initial}
  Show that there is exactly one map $\emptyset \to X$ for any set $X$.
\end{exercise}

\begin{exercise}
  \label{exerc:cardinality-maps}
  Suppose that $X$ is a finite set with $m$ elements and let $n \in \N$.
  Show that $\Map{\fOrd{n}}{X}$ has $m^{n}$ elements.
\end{exercise}




\section{Induction on Natural Numbers}
\label{sec:nat-induction}

We denote by $\N$ the set of \termdef{naturals} starting at $0$.
But what is this set exactly?
Naively, one could define it by saying $\N = \set{0, 1, 2, \dotsc}$.
However, making precise the meaning of the dots is quite difficult.
A better way is to define the natural numbers as some set with a certain property,
as follows.

\begin{definition}{Natural numbers and iteration principle}{natural-numbers}
  The natural numbers are a set $\N$ with an element $0 \in \N$ and a map
  $\mathrm{suc} \from \N \to \N$, such that for any set $A$ with an element
  $a_{0}$ and a map $f \from A \to A$, there is a unique map
  $g \from \N \to A$ such that $g(0) = a_{0}$ and $g(\mathrm{suc}(n)) = f(g(n))$ for all $n \in \N$.
  We say that $g$ is defined by iteration of $(a_{0}, f)$.
\end{definition}

To make our life simple, we will write $n + 1$ instead of $\mathrm{suc}(n)$ for the
successor of the number $n$.
We can the express any natural number by $1 = 0 + 1$, $2 = 1 + 1$, $3 = 2 + 1$ and so forth.
When we wrote $\N = \set{0, 1, 2, \dotsc}$ above, we intuitively understood that
$\N$ should consist of exactly the numbers expressed in this way and that there should
be no spurious elements like $\mathrm{catfish} \in \N$.
That this is the case is expressed by the iteration property because it tells us that
every element in $\N$ is either of the form $0$ or $n + 1$ for some $n \in \N$.
This principle allows us to count from any number down to $0$.
One can say that
\begin{defbox}
  elements of $\N$ are \emph{static} representations of numbers, while iteration reflect the
  \emph{dynamics} of counting.
\end{defbox}

If we consider a pair $(a_{0}, f)$ given as in \cref{def:natural-numbers}, then one can compare
this to the following imperative program, in which a for-loop is used to repeatedly apply $f$ to
the initial value $a_{0}$.
\begin{lstlisting}[language=Python, mathescape=true, style=plstyle]
def g(n):
  res = $a_{0}$
  for i in [1, ..., n]:
    res = f(res)
  return res
\end{lstlisting}

Using the iteration principle can be a nuisance because we have to explicitly specify the
pair $(a_{0}, f)$, which only gets worse for the more complicated structures that we encounter in,
for example, \cref{chap:pl-intro}.
For instance, suppose that we wish to define exponentiation in terms of multiplication.
Our intuition would be to proceed with the following definition.
\begin{equation}
  \label{eq:nn-exponentiation}
  a^{0} = 1 \quad \text{and} \quad a^{n + 1} = a^{n} \cdot a
\end{equation}
This definition arises by applying the iteration principle to the pair $(1, f_{a})$
with $f_{a}(x) = x \cdot a$.
However, defining exponentiation via the formal iteration principle is clearly inconvenient,
and we will use the equational style in \cref{eq:nn-exponentiation} whenever possible
and \emph{we are sure that the equations can be reduced to the iteration principle}.
Such a reduction is not always easy.
For instance, the factorial function can be expressed by the following equations.
\begin{equation}
  \label{eq:nn-factorial}
  0! = 1
  \quad \text{and} \quad
  (n+1)! = n! \cdot (n + 1)
\end{equation}
If we tried to use the iteration principle directly to define $!$ as map $\N \to \N$ via iteration,
then we run into the problem that $n + 1$ is not available for the multiplication.
Instead, we have to define first a map $\mathrm{fac} \from \N \to \N \times \N$ by iteration from
$(1, 1)$ and $f(k, r) = (k + 1,r \cdot k)$.
We then find that $\mathrm{fac}(n) = (n + 1, n!)$.
This shows that the factorial function can be obtained by iteration but this requires considerable
overhead.
Therefore, we typically prefer specifications like in \cref{eq:nn-factorial}.

% For instance, the Ackermann-Péter function $\mathrm{Ack}$ can easily be written in the style of
% \cref{eq:nn-exponentiation}:
% \begin{align*}
%   \mathrm{Ack}(0, n) & = n + 1 \\
%   \mathrm{Ack}(m + 1, 0) & = \mathrm{Ack}(m, 1) \\
%   \mathrm{Ack}(m + 1, n + 1) & = \mathrm{Ack}(m, \mathrm{Ack}(m + 1, n))
% \end{align*}
% The map $\mathrm{Ack}$ can be defined in terms of the iteration principle, but this requires
% us to see it as a map from $\N$ into the set of maps $\N \to \N$.
% How to do this exactly is left to a reader, who is interested in higher-order recursion.

The iteration principle the natural numbers can be used to define most maps on the natural numbers
that we may be interested in.\footnote{Defining exact set of definable maps requires one to set
  up a formal theory, like primitive recursion, Peano arithmetic or Gödel's system T.}
From the iteration principle, we can also derive the usual induction principle.\footnote{We do not
  specify formally at this point what a property is, but we can think of subsets $P \subseteq \N$.}
\begin{theorem}{Induction principle for $\N$}{nn-induction}
  Let $P$ be a property of $\N$, where we write $P(n)$ for the property
  $P$ at $n \in \N$.
  To prove that $P(n)$ holds for all $n \in \N$, it suffices to show that
  \begin{itemize}
  \item $P(0)$ holds (\emphkey{base case}), and
  \item for all $n \in \N$, assuming that $P(n)$ holds, that $P(n+1)$ holds
    (\emphkey{induction step}).
  \end{itemize}
\end{theorem}
\begin{proof}
  The proof of the induction principle needs a concrete definition of what a property is.
  For simplicity, we assume that a property is a subset, that is, $P \subseteq \N$ where
  $P(n)$ means that $n \in P$.
  Our goal is then to show that all natural numbers are contained in $P$.
  In turn, we obtain this by iterating $(0, \mathrm{suc})$ to get a map $g \from \N \to P$
  because $0$ is in $P$ and for any $n \in P$, also $n + 1 \in P$.
  Since $g$ is uniquely defined by $g(0) = 0$ and $g(n+1) = g(n) + 1$, we obtain that
  $\N \subseteq P$, as required.
\end{proof}

The assumption $P(n)$ in the induction step is called the \emphkey{induction hypothesis}, and it
should be noted that it is only assumed for \emph{each individual $n$}.
Sometimes, the induction principle is mistakenly stated with the induction hypothesis separated
as follows.
\begin{defbox}
  \textbf{Incorrect ``induction principle''}:
  To prove that $P(n)$ holds for all $n \in \N$,
  \begin{enumerate}
  \item prove that $P(0)$ holds,
  \item assume that $P(n)$ holds for all $n \in \N$, and
  \item prove that $P(n+1)$ holds for all $n \in \N$.
  \end{enumerate}
\end{defbox}
This, however, is incorrect because the second point implies immediately the first and third,
thereby allowing us to prove that any property!
Clearly, we do not want this and \cref{thm:nn-induction} is the correct principle.
In \cref{chap:fol-intro}, we will see how first order logic allows us to resolve such
ambiguities of natural language.

\section{Trees and Induction}
\label{sec:trees}

There are various kinds of trees: binary trees, which have two children below every node;
lists, which have just one child at any node; trees with arbitrary branching, where every node
may have an arbitrary number of children.
Besides the branching, trees usually have labels.
For instance, in a list every node is labelled with the corresponding list element.
The aim of this section is to give a general account of labelled trees.

We begin by characterising the labels and branching a tree may have.
\begin{definition}{}{branching-type}
  We call a pair $(A, B)$ a \emphkey{branching type} if $A$ is a set and $B$ an $A$-indexed family
  of sets, that is, for every $a \in A$ we are given a set $B_{a}$.
\end{definition}
Note that the terminology of ``branching type'' also tacitly includes labels.
The intuition of this definition is that a tree of type $(A,B)$ will be labelled in $A$ and
will have at a node with label $a \in A$ one branch for every element in $B_{a}$.

\begin{example}{}{binary-branching}
  Binary trees (not balanced!) labelled in $\N$ are induced by the branching type
  $(\N \cup \set{\ast}, B)$ with $B_{n} = \fOrd{2}$ and $B_{\ast} = \emptyset$.
  The idea is that an inner node can be labelled with a number, while a leaf is labelled with $\ast$.
  An inner node has then exactly two children and a leaf has none.
\end{example}

\tikzset{eln/.style={midway, font = \scriptsize, circle, inner sep=1pt, draw=blue, color=blue, yshift=1pt}}

\begin{figure}
  \begin{center}
    \begin{forest}
      for tree={%
%        draw,
%        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$2$
        [$3$, edge label={node[eln,above left]{$0$}}
          [$\ast$, edge label={node[eln,above left]{$0$}}]
          [$10$, edge label={node[eln,above right]{$1$}}
            [$\ast$, edge label={node[eln,above left]{$0$}}]
            [$\ast$, , edge label={node[eln,above right]{$1$}}]
          ]
        ]
        [$0$, edge label={node[eln,above right]{$1$}}
          [$\ast$, edge label={node[eln,above left]{$0$}}]
          [$\ast$, edge label={node[eln,above right]{$1$}}]
        ]
      ]
    \end{forest}
  \end{center}
  \caption[Example of a binary tree]{Example of a binary tree labelled in $\N$.
    The root is labelled with the number $2$ and has two children.
    The circled blue numbers indicate the number of the outgoing branching.
  }
  \label{fig:ex-binary-tree}
\end{figure}
\Cref{fig:ex-binary-tree} shows an example of a binary tree, which we wish to capture with the
branching type given in \cref{ex:binary-branching}.
The following definition shows how general trees can be constructed and what their defining property
is.

\begin{definition}{Trees as inductive structures}{trees}
  Trees with branching type $(A, B)$ are given by a set $\Tr$, or $\Tr(A, B)$, together with a
  family $\tr$ of maps $\tr_{a} \from \Map{B_{a}}{\Tr} \to \Tr$ indexed by elements $a \in A$,
  such that the following iteration principle is fulfilled:
  for any set $X$ and family $f$ of maps with $f_{a} \from \Map{B_{a}}{X} \to X$, there is a
  \emph{unique} map $\bar{f} \from \Tr \to X$ with
  \begin{equation*}
    \bar{f}(\tr_{a}(\alpha)) = f_{a}(g \comp \alpha)
  \end{equation*}
  for all $a \in A$ and $\alpha \from B_{a} \to \Tr$.
  We say that $\bar{f}$ is defined by \emphkey{iteration} from $f$ or that $\bar{f}$ is the
  \emphkey{inductive extension} of $f$.
\end{definition}

This definition packs a lot.
Let us unfold it in the case of binary trees.
\begin{example}{Binary trees}{iteration-binary-trees}
  Let us write $\BTr = \Tr(\N \cup \set{\ast}, B)$ for a set of trees for the branching type that we
  introduced in \cref{ex:binary-branching}.
  This set comes with a family $\tr$ of maps indexed by $\N \cup \set{\ast}$, that is, we get for
  every $n \in \N$ a map $\tr_{n} \from \Map{\fOrd{2}}{\BTr} \to \BTr$ and one map
  $\tr_{\ast} \from \Map{\emptyset}{\BTr} \to \BTr$.
  From the exercises in \cref{sec:sets-maps}, we know that giving a map $\alpha$ in
  $\Map{\fOrd{2}}{\BTr}$ amounts to giving a pair of elements in $\BTr$.
  Thus, we can represent for such an $\alpha$ the resulting tree $tr_{n}(\alpha)$ like so, where a
  box indicates a whole subtree:
  \begin{center}
    \begin{forest}
      for tree={%
%        draw,
%        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$n$
        [$\alpha(0)$, draw, edge label={node[eln,above left]{$0$}}
        ]
        [$\alpha(1)$, draw, edge label={node[eln,above right]{$1$}}
        ]
      ]
    \end{forest}
  \end{center}
  Let us denote by $\ell$ the tree $\tr_{\ast}(\varepsilon)$, where $\varepsilon$ is the only
  element of $\Map{\emptyset}{\BTr}$, see \cref{exerc:empty-initial}.
  This tree represents a leaf in a binary tree.
  The tree from \cref{fig:ex-binary-tree} can then be represented in $\BTr$ by
  \begin{equation*}
    t = \tr_{2}(\tr_{3}(\ell, \tr_{10}(\ell, \ell)), \tr_{0}(\ell, \ell)) \,
  \end{equation*}
  where we use the pair notation to create elements of $\Map{\fOrd{2}}{\BTr}$.

  So much for the construction of tree.
  The interesting part is what we can do with them though.
  This is where the iteration principle comes in, which allows us to traverse a tree.
  For instance, we can sum up all the labels in a tree by using the family $s$ given by
  \begin{align*}
    & s_{n} \from \Map{\fOrd{2}}{\N} \to \N
    && s_{\ast} \from \Map{\emptyset}{\N} \to \N
    \\
    & s_{n} (\alpha) = n + \alpha(0) + \alpha(1)
    && s_{\ast}(\alpha) = 0
  \end{align*}
  this gives us a map $\bar{s} \from \BTr \to \N$.
  Running this map on the tree $t$ from above, we have get the following.
  \begin{align*}
    \bar{s}(t)
    & = s_{2}(\bar{s}(\tr_{3}(\ell, \tr_{10}(\ell, \ell))), \bar{s}(\tr_{0}(\ell, \ell))) \\
    & = 2 + \bar{s}(\tr_{3}(\ell, \tr_{10}(\ell, \ell))) + \bar{s}(\tr_{0}(\ell, \ell)) \\
    & = 2 + (3 + \bar{s}(\ell) + \bar{s}(\tr_{10}(\ell, \ell))) + (0 + \bar{s}(\ell) + \bar{s}(\ell)) \\
    & = 2 + (3 + 0 + (10 + \bar{s}(\ell) + \bar{s}(\ell))) +  (0 + 0 + 0) \\
    & = 2 + (3 + 0 + (10 + 0 + 0)) + 0 \\
    & = 2 + (3 + 0 + 10) + 0 \\
    & = 2 + 13 + 0 \\
    & = 15
  \end{align*}
  Thus, we have traversed the trees depth-first and summed up all the intermediate results.
\end{example}

\begin{exercise}
  Define a map $c \from \BTr \to \N$ that counts the number of labelled nodes in a binary tree by
  using the iteration principle.
\end{exercise}

\begin{exercise}
  Define by iteration a map $h \from \BTr \to \N$ that computes that height of a tree, where the
  leaves should have height $0$ and labelled nodes height at least $1$.
\end{exercise}

So far, have used the iteration principle only to define maps but not to prove anything.
Just like for the natural numbers, we can also obtain an induction principle.

\begin{theorem}{Tree Induction}{tree-induction}
  Let $\Tr$ be a set of tree with branching type $(A, B)$ and $P$ a property of $\Tr$, that
  is $P \subseteq \Tr$.
  If for all $a \in A$ and $\alpha \from B_{a} \to P$ we have $\tr_{a}(\alpha) \in P$, then
  $P$ holds for all trees in $\Tr$ (i.e., $P = \Tr$).
\end{theorem}

\begin{exercise}
  Use the tree induction principle to prove that $c(t) \leq 2^{h(t)}$ for all $t \in \BTr$.
\end{exercise}

\section{Formal Languages}
\label{sec:formal-languages}

Recall that $\Words{A}$ denotes the set of \emph{words} over an alphabet $A$.
Concretely, the set of words is given by
\begin{emphdef}{equation*}
  \Words{A} = \set{\eword} \cup \setDef{a_0 a_1 \dotsm a_n}{n \in \N, a_k \in A},
\end{emphdef}
where $\eword$ is the empty word.
For instance, if $A = \set{a,b}$, then $\Words{A}$ contains the singleton words $a$ and $b$,
and longer words like $abbaa$.
The set of \emph{languages} over $A$ is the powerset $\Langs{A}$, that is,
the set of all subsets of $\Words{A}$.
Given two words $v, w \in \Words{A}$, we denote by $vw$ or $v \conc w$ the \emphkey{concatenation}
of the words $v$ and $w$, that is, considering $v$ and $w$ as one word by reading their letters
in order.
\begin{equation*}
  (v_{0}\dotsm v_{n}) \conc (w_{0} \dotsm w_{m}) = v_{0}\dotsm v_{n}w_{0} \dotsm w_{m}
\end{equation*}

We will often make use of \emphkey{context-free grammars}, which generate languages.
These grammars will be notated in so-called \emphkey{Backus-Naur form}.
Let us start with an example.

\begin{example}{}{cfg-arithmetic}
  Suppose we want to define a language of arithmetic expressions, in which one can use
  numbers in $\N$, addition and negation.
  In this case, we would say that such expressions $e$ are generated by the following
  grammar.
  \begin{equation}
    \label{eq:cfg-arithmetic}
    e \cce n  \, , n \in \N \mid e + e \mid -e \mid (e)
  \end{equation}
  This grammar can be read as follows.
  The symbol $\cce$ says that an expression $e$ can be generated by using any of the
  four options on the right-hand side, where the options are separated by the vertical bar.
  The first option is that $e$ can be any natural number $n$, which is the only case where
  we can start to generate an expression.
  To generate larger expressions, we have to use any of the other two options.
  For instance, if we have generated already expressions $e_{1}$ and $e_{2}$, then the second option
  allows us to generate the expression ``$e_{1} + e_{2}$'', the third option gives us
  ``$- e_{1}$'' and the fourth introduces parentheses ``$(e_{1})$''.
  It is important to realise that ``$+$'' and ``$-$'' have no meaning, they are just syntax.
  In the terminology of (context-free) grammars, ``$($'', ``$)$'', ``$+$'', ``$-$'' and ``$n$''
  are called \emphkey{terminal symbols}, while $e$ in the grammar is called a
  \emphkey{non-terminal symbol}.

  We can now define the language generated by the grammar, call it $L$, as a subset of all
  words over the alphabet $A = \N \cup \set{+, -, (, )}$ by
  \begin{equation*}
    E = \setDef{e \in \Words{A}}{\text{ generated by \cref{eq:cfg-arithmetic}}} \, .
  \end{equation*}
  But what does ``generated by'' mean exactly?
  We can think of \cref{eq:cfg-arithmetic} as a way of describing trees of a certain shape.
  For instance, the expression $5 + (-3)$ can be seen as a linear, textual description of the
  following tree.
  \begin{center}
    \begin{forest}
      for tree={%
%        draw,
%        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$+$ [$5$] [$()$ [$-$ [$3$]]]]
    \end{forest}
  \end{center}
  In \cref{sec:trees}, we have already seen how to describe such trees.
  The labels are numbers and the operators, that is, we put
  \begin{equation*}
    L = \N \cup \set{+, -, ()}
  \end{equation*}
  and the branching width $B$ is given by
  \begin{equation*}
    B_{n} = \emptyset \qquad B_{+} = \fOrd{2} \qquad B_{-} = \fOrd{1} \qquad B_{()} = \fOrd{1} \, ,
  \end{equation*}
  which indicates that the numbers are leaves, ``$+$'' has two children, and ``$-$'' and ``$()$''
  have one.
  An expression can be seen as a tree of branching type $(L, B)$.
  To get a language, we define a map $\mathrm{flat} \from \Tr(L,B) \to \Words{A}$ by iteration
  of the family $\set{f_{x}}_{x \in L} \from \MapNP{B_{x}}{\Words{A}} \to \Words{A}$ defined by
  \begin{align*}
    f_{n}(\alpha) & = n \\
    f_{+}(\alpha) & = \alpha(0) \conc "+" \conc \alpha(1) \\
    f_{+}(\alpha) & = "-" \conc \alpha(0) \\
    f_{()}(\alpha) & = "(" \conc \alpha(0) \conc ")" \\
  \end{align*}
  This gives us that expressions are given as the image of the map $\mathrm{flat}$:
  \begin{equation*}
    E = \setDef{\mathrm{flat}(t)}{t \in \Tr(L, B)}
  \end{equation*}

  There is something peculiar about trees compared to expressions: The latter need parentheses
  to disambiguate, as we do not know how to generate the word $5 + 4 + 1$ with our grammar
  and there are two different trees that $\mathrm{flat}$ maps to this word:
  \begin{center}
    \begin{forest}
      for tree={%
%        draw,
%        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$+$ [$5$] [$+$ [$4$] [$1$]]]
    \end{forest}
    \begin{forest}
      for tree={%
%        draw,
%        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$+$ [$+$ [$5$] [$4$]] [$1$]]
    \end{forest}
  \end{center}
  To resolve this ambiguity, we normally denote these expressions by, respectively,
  $5 + (4 + 1)$ and $(5 + 4) + 1$.
  This tells us that trees do not need the parentheses and all ambiguity is removed.
  In fact, we can see this already in the branching type, in which the parentheses do not
  add any branching and merely reflect the parentheses in the grammar.
  Often parentheses can be left out by constructing a grammar more cleverly than what we
  did, but we leave that for our specific uses of grammars.
\end{example}

\begin{definition}{}{cfg}
  Let $A$ be an alphabet (a set).
  A \emphkey{context-free grammar} $G$ over $A$ is a tuple $(V, R)$ where
  $V$ is a finite set of non-terminal symbols and $R \subseteq V \times (A \cup V)$ is
  a relation, the \emphkey{production rules} of $G$.
\end{definition}

\begin{example}{}{cfg-arithmetic-formal}
  Taking $V = \set{e}$ and
  \begin{equation*}
    R = \setDef{(e, n)}{n \in \N} \cup \set{(e, e+e)} \cup \set{(e, -e)} \cup \set{(e, (e))}
  \end{equation*}
  is the grammar given in \cref{ex:cfg-arithmetic}.
\end{example}

% \section{Indexed Trees and Parsing Computations (optional)}
% \label{sec:indexed-trees}

% In \cref{sec:trees}, we have introduced trees with an iteration principle that allowed for arbitrary
% labelling and branching.
% For many purposes, this is fine but often we need to restrict trees with some conditions.
% One example of this will be proof trees, where the trees that may appear under one branch are
% restricted by the label of node from which the branch originates.
% To be more concrete, let us look how parsing of expressions via grammar works operationally.

% \Cref{ex:cfg-arithmetic} shows how expressions can be represented as trees, but one can even
% represent the generating process for each words from a given grammar by a tree that records
% the computation steps.
% Suppose we are given the following grammar over the alphabet $\set{a,b,c}$ and with
% non-terminals $\set{X, Y}$.
% \begin{align*}
%   X & \cce Y a Y \\
%   Y & \cce b X Y \mid c
% \end{align*}
% To interpret this grammar, we can consider trees that display how a word is generated by the
% grammar.
% Suppose that we start at $X$, then here is a possible tree that shows applications of
% productions rules to generate a word.
% \begin{center}
%   \begin{forest}
%     for tree={%
%       l sep=1mm,
%       s sep=10mm
%     }
%     [{$(X, YaY)$}
%     [{$(Y, bXY)$}
%       [{$(X, YaY)$} [{$(Y,c)$}] [{$(Y,c)$}]]
%       [{$(Y,c)$}]
%     ]
%     [{$(Y, c)$}]
%     ]
%   \end{forest}
% \end{center}
% The word that this trees represents is ``$bcaccac$''.
% But how do we get such a tree and read off the word?

% Given maps $f \from X \to I$ and $g \from Y \to I$ into a common set $I$,
% let us write $R(f, g)$ for the maps from $X$ to $Y$ that respect $f$ and $g$:
% \begin{equation*}
%   R(f,g) = \setDef{h \from X \to Y}{g \comp h = f} \, .
% \end{equation*}
% This will allow us to define how branching respects the index of a tree.

% \begin{definition}{}{indexed-branching}
%   Let $I$ be a set.
%   An \emphkey{$I$-indexed branching type} is a tuple $(A, B, p, q)$, where
%   $(A, B)$ is a branching type, $p \from A \to I$ a map and $q$ an $A$-indexed
%   family of maps $q_{a} \from B_{A} \to I$.
%   \emphkey{$I$-indexed trees} for such a branching type are given by a tuple $(\Tr, i, \tr)$,
%   such that
%   \begin{enumerate}
%   \item $i \from \Tr \to I$ maps trees to their index,
%   \item $\tr_{a} \from R(q_{a}, i) \to \Tr$ is a map with $i(\tr_{a}(\alpha)) = p(a)$, and
%   \item if $(X, s, f)$ is a tuple where $X$ is a set, $s \from X \to I$ and $f$ is a family
%     of maps $f_{a} \from R(q_{a}, s) \to X$ with $i(f_{a}(\alpha)) = p(a)$, then there
%     is a unique map $\bar{f} \in R(i, s)$.
%   \end{enumerate}
% \end{definition}

% \begin{definition}{}{cfg-computation}
%   Let $G = (V, R)$ be a grammar.
%   The accepting computations for $G$ are the $V$-indexed trees
%   $\Tr(R, B^{G}, p, q) = (\Tr, i, \tr)$\todo{Better notation}, where $p(X, w) = X$,
%   \begin{equation*}
%     B^{G}_{(X, w)} = \setDef{(k,Y)}{Y \text{ is the kth non-terminal in } w} \, ,
%   \end{equation*}
%   and $q_{(X, w)}(k, Y) = Y$.

%   If a non-terminal $X \in V$ is chosen, the computations starting in $X$ are
%   those trees with $i(t) = X$.
% \end{definition}

% \todo[inline]{Add example from above with full annotation.
%   Demonstrate UMP to get generated word out of a tree.}



\chapter{Three-Valued Logic}
\label{chap:3-valued-logic}

\newcommand*{\Tri}{\mathbb{T}}
\newcommand*{\Tleq}{\sqsubseteq}

In what follows, we describe the so-called 3-valued Heyting logic or algebra.
Let $\Tri$ be the set $\set{\FF, X, \TT}$.
Intuitively, we understand $\FF$ and $\TT$ as true and false, as in \cref{chap:pl-semantics}, while
the third element $X$ of $\Tri$ should be seen as an unknown truth value.
This can occur, for example, in computer when the voltage of a logical signal is not high enough or
fluctuates and thereby creates an undefined logic state.
We will see that $\Tri$ can be used a domain for modelling propositional logic.
First of all, we define an order $\Tleq$ on $\Tri$ by
\begin{equation*}
  \FF \Tleq X \, ,
  \quad X \Tleq \TT \, ,
  \quad \FF \Tleq \TT \, ,
  \quad \FF \Tleq \FF \, ,
  \quad X \Tleq X
  \quad \text{ and }
  \quad \TT \Tleq \TT \, .
\end{equation*}
This order allows us to use $\min$ and $\max$ as usual, and if we use them to interpret conjunction
and disjunction, then we will see that they conform to our expectation of an unknown value as input
to logic gates:

\begin{center}
  $\min(a,b)$ (interpretation of $\land$) and $\max(a,b)$ (interpretation of $\lor$) \\
  \begin{tabular}{c|ccc}
    \diagbox[height=1.7em]{$a$}{$b$}
        & 0 & X & 1 \\ \hline
    0   & 0 & 0 & 0 \\
    X   & 0 & X & X \\
    1   & 0 & X & 1 \\
  \end{tabular}
  \qquad
  \begin{tabular}{c|ccc}
    \diagbox[height=1.7em]{$a$}{$b$}
        & 0 & X & 1 \\ \hline
    0   & 0 & X & 1 \\
    X   & X & X & 1 \\
    1   & 1 & 1 & 1 \\
  \end{tabular}
\end{center}

% \begin{center}
%   \begin{tabular}{cc|cc}
%     $a$ & $b$ & $\min(a,b)$ & $\max(a,b)$ \\ \hline
%     0 & 0 & 0 & 0 \\
%     0 & 1 & 0 & 1 \\
%     1 & 0 & 0 & 1 \\
%     1 & 1 & 1 & 1 \\
%     X & 0 & 0 & X \\
%     X & 1 & X & 1 \\
%     0 & X & 0 & X \\
%     1 & X & X & 1
%   \end{tabular}
% \end{center}

We can also define a semantic implication $\semImpl_{\Tri}$ on $\Tri$ just as we did
for the Boolean semantics:
\begin{equation*}
  a \semImpl_{\Tri} b =
  \begin{cases}
    \TT, & a \Tleq b \\
    b, & \text{otherwise}
  \end{cases}
\end{equation*}
The following table lists all the possibilities for $\semImpl_{Tri}$ and the resulting negation:
\begin{center}
  Tables of $a \semImpl_{\Tri} b$ and negation of $a$ \\
  % \begin{tabular}{c|ccc}
  %   \diagbox[height=1.7em]{$b$}{$a$}
  %       & 0 & X & 1 \\ \hline
  %   0   & 1 & X & 1 \\
  %   X   & X & X & X \\
  %   1   & 0 & 1 & 1 \\
  % \end{tabular}
  \begin{tabular}{c|ccc}
    \diagbox[height=1.7em]{$a$}{$b$}
        & 0 & X & 1 \\ \hline
    0   & 1 & 1 & 1 \\
    X   & 0 & 1 & 1 \\
    1   & 0 & X & 1
  \end{tabular}
  % \begin{tabular}{cc|c}
  %   $a$ & $b$ & $a \semImpl_{\Tri} b$ \\ \hline
  %   0 & 0 & 1 \\
  %   0 & 1 & 1 \\
  %   1 & 0 & 0 \\
  %   1 & 1 & 1 \\
  %   X & 0 & X \\
  %   X & 1 & 1 \\
  %   0 & X & 1 \\
  %   1 & X & X
  % \end{tabular}
  \quad
  \begin{tabular}{c|c}
    a & $a \semImpl_{\Tri} \FF$ \\ \hline
    0 & 1 \\
    X & 0 \\
    1 & 0
  \end{tabular}
\end{center}

Putting this all together, we can define for valuations $v \from \PVar \to \Tri$ a map
$\sem{-}^{\Tri}_{v} \from \PForm \to \Tri$ analogously to \cref{def:pl-semantics}.
We can use this map to also gives us an entailment relation $\vDash_{\Tri}$ by defining
\begin{equation*}
  \Gamma \vDash_{\Tri} \phi \text{ if for all v we have }
  \sem{\Gamma}^{\Tri}_{v} \leq \sem{\phi}^{\Tri}_{v} \, .
\end{equation*}

Also similarly to the Boolean model (\cref{thm:pl-nd-soundness}), one can prove the following
soundness result.
\begin{theorem}{}{pl-nd-tri-sound}
  If $\seq{\Gamma}{\phi}$ is derivable in \ND{}, then $\Gamma \vDash_{\Tri} \phi$.
\end{theorem}

However, the similarity with the Boolean model stops when we move to classical logic.
Indeed, it is easy to see that $\not\vDash_{\Tri} p \lor \neg p$.
Let $v$ be the valuation that is equal to $X$ everywhere.
We then have
\begin{equation*}
  \sem{p \lor \neg p}_{v}
  = \max\set{\sem{p}_{v}, \sem{\neg p}_{v}}
  = \max\set{X, 0}
  = X
  \neq \TT \, .
\end{equation*}
This shows that $\neg\vDash_{\Tri} p \lor \neg p$ and therefore the \Contra{}-rule from
\cref{def:pl-cnd} cannot be sound for this three-valued model.

There are other possibilities for interpreting the implication, see Łukasiewicz's or Kleene's
three-valued logic~\cite[\S~64]{Kleene74:IntroductionMetamathematics}, but different proof
systems than \ND{} and \cND{} are needed to handle those interpretations.



\chapter{Logic Programming}
\label{chap:logic-programming}

\lstset{language=Prolog,style=plstyle}
\lstinputlisting[
emph={shortest,adjacent,step,connr, robot, goal, route, free},
emph={[2]conn,path}]
{code/robot-forward.pl}


\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: