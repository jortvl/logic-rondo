\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{2}{Introduction to Propositional Logic}{pl-intro}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motivation}
\label{sec:pl-motivation}

\begin{dialogue}
  \speak{Isaac} Ok, I see why you would want to study logic. But where do we start?
  \spC I have an idea. Let us go to the cradle of philosophy and mathematics in Europe.
  \speak{Aristotle} Who are you, entering my house with that metal construction?
  \speak{Isaac} I'm not a metal construction! I'm a living and breathing robot!
  \speak{Aristotle} By Zeus! It can speak!
  \speak{Isaac} More than that: without my Trans-O-Matic you would not be able to understand
  me or my friend.
  \speak{Aristotle} A robot, mh? What is that supposed to mean?
  \spC Of course, you cannot know. You may think of an αὐτόματον (automaton) as Hephaestus
  created them, only that a robot can make decisions for itself and act like a human, all within
  limits certainly.
  \speak{Aristotle} How fascinating! May I keep you here for studying my friend?
  \speak{Isaac} Isaac is the name and I would prefer to be studied, if you don't mind.
  Rather, Clara and I are trying to understand the roots of my being together.
  We decided to start of quest with studying the logic underlying my artificial brain and
  my decisions.
  But we are not sure where to start. That's why we are here with you, one of the
  founders of formal logic.
  \speak{Aristotle} I see, I see. Start where all logic starts: with simple deduction.
  Take, for example, the following deduction.
  ``If it rains and Socrates has no umbrella, then he gets wet.
  Socrates has no umbrella and is not wet.
  Therefore, it does not rain.''
  \spC Do you always have to use old male Philosophers as examples?
  \speak{Aristotle} These are very illustrative examples, are they not?
  \spC Well\dots{} Never mind!
  The point is thus, that we infer knowledge from hypotheses and facts?
  \speak{Aristotle} Yes, exactly.
  We shall therefore begin with the study of a simple logic that allows us to express
  the relation between propositions and infer knowledge from these relations.
  Let me introduce you to the syntax of a logic, as modern logic more than 2000 years
  from now will understand it.
\end{dialogue}

This logic is called \emph{propositional logic}, and comprises \emph{propositional variables} and
\emph{logical connectives} that allow us to express relations between these variables as \emph{formulas}.
In \cref{sec:pl-syntax}, we will see how these formulas are formed precisely and how we can
use them to formalise hypotheses and facts.

For the time being, we will help Isaac to identify the relevant fragments of Aristotle's example.
Let us \emphkey{highlight} in the example the propositions that can be either true or false.
\begin{quote}
  ``If \emphkey{it rains} and \emphkey{Socrates has no umbrella}, then \emphkey{he gets wet}.
  \emphkey{Socrates has no umbrella} and \emphkey{he does not get wet}.
  Therefore, \emphkey{it does not rain}.''
\end{quote}
If we write instead
\begin{itemize}
\item $r$ for \emphkey{it rains},
\item $u$ for \emphkey{Socrates has no umbrella}, and
\item $w$ for \emphkey{he gets wet},
\end{itemize}
then we can easily rewrite the first sentence to
\begin{quote}
  ``If $r$ and $u$, then $w$.''
\end{quote}
and can read it still in exactly the same way by replacing $r$, $u$ and $w$ by the corresponding
phrases.
We call $r$, $u$ and $w$ \emph{propositional variables}, as they stand for propositions that can
be either true or false.
This is very important to keep in mind: we reason about such variables independently of their
truth, and arguments need to be able to account for any possibility!

You will have noticed that the second sentence cannot directly written with the three propositional
variables because it says ``he does \emph{not} get wet'', while we only have the variable $w$ that
stands for ``he gets wet''.
We will allow ourselves to write ``not'' in front of a variable to negate what it says, that is,
``not $w$'' stands for ``he does \emph{not} get wet''.
In natural language, words have to appear in one or another order.
This can create ambiguities, which is exactly what formal logic tries to prevent.
With this in mind, we can write the second and third sentence as
\begin{quote}
  ``$u$ and not $w$.
  Therefore, not $r$.''
\end{quote}
What is left of the original sentence are only the words ``if'', ``then'', ``and'', ``therefore''
and ``not''.
These are \emph{logical connectives}, and we will introduce formal notations for them soon.

But let us appreciate for a moment that we have replaced in the original example certain phrases
by variables and obtained an argument that relies only on the \emph{structure} of the sentences,
rather than their \emph{meaning}.
For instance, we could reinterpret the variables like so:
\begin{itemize}
\item $r$ --- \emphkey{Isaac's battery is empty},
\item $u$ --- \emphkey{no charger is in reach}, and
\item $w$ --- \emphkey{Isaac stops working},
\end{itemize}
This gives us another argument that is clearly valid:
\begin{quote}
  ``If \emphkey{Isaac's battery is empty} and \emphkey{no charger is in reach},
  then \emphkey{Isaac stops working}.
  \emphkey{No charger is in reach} and \emphkey{Isaac does not stop working}.
  Therefore, \emphkey{Isaac's battery is not empty}.''
\end{quote}

\section{Syntax of Propositional Logic}
\label{sec:pl-syntax}

Syntax is the pillar of formal logic that allows us to express propositions \emph{unambiguously}.
As we saw earlier, it consists for propositional logic of two parts: propositional variables
and logical connectives that can be put together into formulas.
Propositional variables are syntactic entities that represent propositions, but have
\emph{no intrinsic meaning} and only serve as \emph{placeholders}.
We shall assume to be given a fixed set of such variables.

\begin{assumption}{}{pl-variables}
  Assume that $\PVar$ is a countable set of \emph{propositional variables}.
  Elements of $\PVar$ are denoted by lower-case letters, possibly with index:
  $p, q, \dotsc, p_{0}, p_{1}, \dotsc$
\end{assumption}

As for the logic connectives, we will introduce symbols and formulas that allow us to unambiguously
express propositions.
For instance, we will write $\land$ for ``and'' and $\neg$ for ``not''.
With these notations and the variables $u$ and $w$ from above, the phrase
\begin{quote}
  ``Socrates has no umbrella and he does not get wet.''
\end{quote}
becomes
\begin{equation*}
  u \land \neg w \, .
\end{equation*}
Note that there are cases, where it is not clear how a sentence has to be read and we may have to
use parentheses to disambiguate the reading.
For example, we will write $\to$ for ``if \dots then \dots''.
Then the sentence
\begin{quote}
  ``If it rains and Socrates has no umbrella, then he gets wet.''
\end{quote}
can be written as
\begin{equation*}
  (r \land u) \to w \, .
\end{equation*}
However, such parentheses can get in the way and we would like to have some reading conventions.
We read, for example, the proposition $u \land \neg w$ intuitively already as $u \land (\neg w)$.
Thus, part of the definition of formulas will also be a reading convention that allows us to
unambiguously determine the structure of formulas.

\begin{definition}{}{pl-syntax}
  The well-formed formulas (wff) $\phi$ of propositional logic are generated by the following context
  free grammar, in which $p$ ranges over the propositional variables $\PVar$.
  \begin{equation*}
    \phi
    \cce p
    \mid \bot
    \mid \phi \land \phi
    \mid \phi \lor \phi
    \mid \phi \to \phi
    \mid (\phi)
  \end{equation*}
  Ambiguities in the grammar are resolved by the use of parentheses and the following reading
  conventions.
  \begin{itemize}
  \item $\land$ and $\lor$ have precedence over $\to$
  \item all connectives associate to the right
  \end{itemize}
  The set of all propositional wff is denoted by $\PForm$.
  We denote elements of $\PForm$ by small Greek letters $\phi, \psi, \gamma, \dotsc$ possibly
  with a subscript index.
\end{definition}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{c|l|l|l}
      Connective & Name & Pronunciation & Intuitive Meaning \\ \hline
      $\bot$  & \Gls{absurdity} & bottom & $\bot$ never holds \\
      % $p, q, \dotsc$ & Propositional variable \\
      $\land$ & \Gls{conjunction} & $\phi$ and $\psi$ & both hold \\
      $\lor$  & \Gls{disjunction} & $\phi$ or $\psi$ & $\phi$ or $\psi$ or both hold \\
      $\to$   & \Gls{implication} & $\phi$ implies $\psi$ & if $\phi$ holds, then $\psi$ holds
    \end{tabular}
  \end{center}
  \caption[Propositional connectives]{Logical connectives of propositional logic}
  \label{tab:pl-connectives}
\end{table}
As reading and understanding formulas can be difficult, especially on first sight, let me provide
some help.
In \cref{tab:pl-connectives}, all the connectives with their name, pronunciation and intuitive
meaning are gathered.
The name is how we will refer to a connective by itself, outside of formulas.
To pronunciation we will use, of course, the pronunciation column.
If you have trouble with Greek letters, then have a look at \cref{chap:greek}.
Finally, the last column indicates how the connectives can be understood intuitively.
Keep in mind though that this is only an intuition and the interpretation can vary
radically for different applications and semantics, one of which we will discuss in
\cref{chap:pl-semantics}.

``But wait'', Clara intervenes, ``we are missing the negation, aren't we?''
Yes indeed, we are.
However, our intuition would dictate that the formula $\neg \phi$ should hold only if $\phi$ does
not hold.
Or, in other words, whenever $\phi$ holds, something went wrong and we discovered an absurd
situation.
We can express this by the formula $\phi \to \bot$, saying that $\phi$ implies absurdity.
Similarly, we can also express other common logical connectives in terms of the basic
connectives.
\begin{definition}{Derived connectives}{pl-derived-connectives}
  We define three derived connectives as short-hand notation for the formula in second column of the
  following table.
  \begin{center}
    \begin{tabular}{c|c|l}
      Connective & Definition & Name \\ \hline
      $\neg \phi$ & $\phi \to \bot$  & \Gls{negation} \\
      $\top$      & $\neg \bot$ & \Gls{syn:truth} or Top \\
      $\phi \bimpl \psi$ & $(\phi \to \psi) \land (\psi \to \phi)$
      & \Gls{bimpl}
    \end{tabular}
  \end{center}
  We also adopt the following reading conventions:
  negation has precedence over $\lor$ and $\land$, thus also over $\to$; and bi-implication has
  the same precedence as $\to$.
\end{definition}

Let us now come back to the original example.

\begin{example}{}{pl-form-aristotle}
  The deduction that it did rain by experimenting with Socrates' misery consists of
  the following three formulas.
  \begin{gather*}
    r \land u \to w
    \quad \text{and} \quad
    u \land \neg w
    \quad \text{and} \quad
    \neg r
  \end{gather*}
  Putting these all together as one deduction, we obtain:
  \begin{equation*}
    (r \land u \to w)
    \land
    (u \land \neg w)
    \to
    \neg r
  \end{equation*}
  Note that the first two formulas are put together with a conjunction,
  while the last comes after an implication.
  This way, we have resolved all the ambiguity in the original deduction.
  The sentence starting with ``Therefore'' signifies the right-hand side, the \emph{conclusion} of
  an implication and everything before are the \emph{assumptions} that are made in the deduction.
\end{example}

You should also appreciate that we can leave out parentheses by using our reading conventions.
Without them, the formula in \cref{ex:pl-form-aristotle} would look like this:
\begin{equation*}
  \parens[\Bigg]{
    \parens[\Big]{
      (r \land u) \to w
    }
    \land
    \parens[\Big]{
      u \land (\neg w)
    }
  }
  \to
  (\neg r)
\end{equation*}
What an abomination!
I wish, I had these tools in the debates with my contemporary philosophers in the ancient Greek
times.

\Cref{tab:pl-precedences-ex} shows some more example, in which the reading conventions allow us to
leave out parentheses
\begin{table}
  \center
  \begin{tabular}{c|c}
    Formula             & With parentheses \\
    \hline
    $p \to q \to r$     & $p \to (q \to r)$ \\
    $p \land q \land r$ & $p \land (q \land r)$ \\
    $p \lor q \lor r$   & $p \lor (q \lor r)$ \\
    \hline
    $p \land q \to r$   & $(p \land q) \to r$ \\
    $p \lor q \to r$    & $(p \lor q) \to r$ \\
    $p \to q \land r$   & $p \to (q \land r)$
  \end{tabular}
  \caption{Leaving out parentheses by using the precedences of connectives}
  \label{tab:pl-precedences-ex}
\end{table}
Note that there is no convention about mixing $\land$ and $\lor$, as this would cause
more confusion than it helps.
For example, the formula $p \land q \lor r$ is considered to be ambiguous and should be written
either as $(p \land q) \lor r$ or $p \land (q \lor r)$.

\section{Parse Trees}
\label{sec:pl-parse-trees}

\begin{dialogue}
  \speak{Isaac} I have the feeling that there may still be some ambiguity.
  How can I know in which order I have to process a formula?
  \speak{Aristotle} Feelings? How \dots{}?
  \speak{Isaac} Hey, no need to insult me!
  \speak{Aristotle} My apologies! But you are a curious thing and I would like to ask you so many
  questions.
  In any case, there is a way to make everything absolutely unambiguous by two-dimensional trees,
  the kind you have seen as data structures.
\end{dialogue}

\begin{table}[ht]
  \centering
  \begin{tabular}{c|c|c}
    Formula & Top-Level Connective & Direct Subformulas \\ \hline
    $p$ & $p$ & -- \\
    $\bot$ & $\bot$ & -- \\
    $\phi \land \psi$ & $\land$ & $\phi, \psi$ \\
    $\phi \lor \psi$ & $\lor$ & $\phi, \psi$ \\
    $\phi \to \psi$ & $\to$ & $\phi, \psi$ \\
  \end{tabular}
  \caption{Top-Level Connectives and Direct Subformulas}
  \label{tab:direct-subformulas}
\end{table}
\begin{definition}{}{pl-parse-trees}
  The \emph{top-level connective} and \emph{direct subformulas} of formulas are given
  as in \cref{tab:direct-subformulas}.
  A formula is called \emph{atomic} if it has no direct subformulas, that is, if it is of the shape
  $\bot$ or $p$ for $p \in \PVar$.

  Given a formula $\phi$, the \emph{parse tree} of $\phi$ is a tree, in which
  \begin{enumerate}[label=\roman*)]
  \item the root is labelled by the top-level connective of $\phi$, and
  \item the children of the root are parse trees of the direct subformulas of $\phi$.
  \end{enumerate}
\end{definition}

When we picture such trees, we typically draw a circle for every node and write the label
inside this node.
This allows to picture, for example, the formula $p \land q$ as
\begin{center}
  \begin{forest}
    for tree={%
      draw,
      circle,
      l sep=1mm,
      s sep=10mm
    }
    [$\land$
      [$p$]
      [$q$]
    ]
  \end{forest}
\end{center}

Our formula from earlier can serve as a more elaborate example.
\begin{example}{}{pl-form-aristotle-tree}
  Recall the formula
    \begin{equation*}
    (r \land u \to w)
    \land
    (u \land \neg w)
    \to
    \neg r
  \end{equation*}
  from \cref{ex:pl-form-aristotle}.
  The parse tree of this formula is given as follows.
  \begin{center}
    \begin{forest}
      for tree={%
        draw,
        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$\to$
        [$\land$
          [$\to$
            [$\land$ [$r$] [$u$] ]
            [$w$]
          ]
          [$\land$
            [$u$]
            [$\to$ [$w$] [$\bot$]]
          ]
        ]
        [$\to$ [$r$] [$\bot$]]
      ]
    \end{forest}
  \end{center}
  Note that the parse tree does not contain negations because this is a derived connective.
  Instead, it is represented by the defining formula.
  For instance, $\neg r$ becomes $r \to \bot$ in the parse tree.
\end{example}

With parse trees, we can clearly circumvent any ambiguities.

\section{Formula Iteration and Induction}
\label{sec:pl-formula-induction}

\begin{dialogue}
  \speak{Isaac} What a great contribution of computer science to the world!
  But what If I would like to make any formal statements or definitions for formulas? Are
  parse trees then not a bit too informal?
  \speak{Aristotle} In fact, they are not. As parse trees give a unique representation for formulas,
  we can derive a proof principle that is familiar from the natural numbers.
  \spC Are you speaking about induction?
  \speak{Aristotle} Indeed, I am.
  However, let me state first the iteration principle for formulas, as this allows us to define
  maps on formulas.
\end{dialogue}

\begin{theorem}{Principle of Formula Iteration}{pl-formula-iteration}
  Let $A$ be a set together with $f_{\bot} \in A$ and four maps
  \begin{align*}
    & f_{P} \from \PVar \to A
    & f_{\land} \from A \times A \to A \\
    & f_{\lor} \from A \times A \to A
    & f_{\to} \from A \times A \to A \, ,
  \end{align*}
  where $A \times A$ is the set-theoretic product (\cref{sec:sets-maps}).
  Then there is a unique map $f \from \PForm \to A$, such that the
  following equations hold.
  \begin{align*}
    f(\bot) & = f_{\bot}
    & f(\phi \land \psi) & = f_{\land}(f(\phi), f(\psi)) \\
    f(p)    & = f_{P}(p)
    & f(\phi \lor \psi) & = f_{\lor}(f(\phi), f(\psi)) \\
    & & f(\phi \to \psi) & = f_{\to}(f(\phi), f(\psi))
  \end{align*}
\end{theorem}
\begin{proof}
  The idea of the proof is simple.
  Given the element $f_{\bot}$ and the four maps, we define $f(\phi)$ by traversing the
  parse tree $T$ depth-first from left to right.
  This, in turn, is done by iteration on the height of the tree $T$.

  For atomic formulas, $T$ has height $0$ and we can directly define
  $f(\phi) = f_{\bot}$ ($\phi = \bot$) or $f(\phi) = f_{P}(\phi)$ ($\phi \in \PVar$).
  If $\phi$ is not atomic, for example, if $\phi = \phi_{1} \land \phi_{2}$, then
  $T$ is labelled at the root with $\land$ and the root has trees $T_{1}$ and $T_{2}$ as children.
  These children are themselves parse trees of smaller height and we can assume $f(\phi_{k})$
  to be given.
  Thus, we can define $f(\phi) = f_{\land}(f(\phi_{1}), f(\phi_{2}))$.
  Clearly, $f$ fulfils the required equations.

  If we are given a map $g \from \PForm \to A$ that also fulfils the equations, then we can
  prove that $f = g$ also by induction on the height of parse trees.
  For atomic formulas $\phi$, we clearly have $g(\phi) = f(\phi)$.
  If $\phi$ has a parse tree of height $n + 1$, say, $\phi = \phi_{1} \land \phi_{2}$, then we can
  assume as induction hypothesis that $g(\phi_{k}) = f(\phi_{k})$ for $k = 1,2$.
  But then
  \begin{equation*}
    g(\phi) = f_{\land}(g(\phi_{1}), g(\phi_{2}) = f_{\land}(f(\phi_{1}), f(\phi_{2}) = f(\phi).
  \end{equation*}
  Thus, by induction on the tree height of $\phi$, we have that $g(\phi) = f(\phi)$ for all formulas
  $\phi$ and thereby that $f$ is unique.
\end{proof}

Just like for natural numbers (\cref{thm:nn-induction}), the usual induction principle can be
obtained as a special case of the iteration principle.
\begin{corollary}{Formula Induction}{pl-formula-induction}
  Let $P$ be a property of formulas, that is, $P \subseteq \PForm$.
  If
  \begin{enumerate}[label=\roman*)]
  \item \label{pl-formula-base} $P$ contains all atomic formulas ($\PVar \subseteq P$ and $\bot \in P$), and
  \item \label{pl-formula-step} for all formulas $\phi$: if $P$ contains all direct subformulas of $\phi$, then
    $\phi \in P$,
  \end{enumerate}
  then $P$ contains all formulas ($\PForm \subseteq P$).
  We refer to \cref{pl-formula-base} as the \emphkey{base case} and to
  \cref{pl-formula-step} as the \emphkey{induction step}.
\end{corollary}
% \begin{proof}
%   The idea of the proof is simple.
%   Given a property $P$ that fulfils the two above properties, we have to show that all formulas
%   $\phi$ are contained in $P$.
%   Let us assume that $\phi$ has a parse tree $T$.
%   We proceed by induction on the height of the tree $T$.

%   If $\phi$ is atomic, then $\phi \in P$ by the base case.
%   If $\phi$ is not atomic, for example, if $\phi = \phi_{1} \land \phi_{2}$, then
%   $T$ is labelled at the root with $\land$ and the root has trees $T_{1}$ and $T_{2}$ as children.
%   These children are themselves parse trees of smaller height.
%   Thus, by induction (on natural numbers), we know that the formulas $\phi_{1}$ and $\phi_{2}$
%   are both in $P$.
%   By the induction step of $P$, property \cref{pl-formula-step}, we then obtain that also
%   $\phi \in P$.
% \end{proof}

As you can see, formula iteration and induction follow directly from the fact that formulas can be
represented as parse trees with finite height.
However, formula iteration and induction are much easier to use than induction on the height of
trees because we can directly refer to the structure of the tree.
This is why induction principles like that in \cref{cor:pl-formula-induction} are also referred to
as \emph{structural induction}.

What can we use these principles for?
For example, we can use it to find \emph{all} subformulas, and not just the direct ones, of a given
formula.\todo{Add $\var$ example}
\begin{definition}{}{pl-subformulas}
  Let $\phi$ be a formula.
  The set of \termdefpl{subformula} of $\phi$ is given by the following iterative definition.
  \begin{align*}
    \Sub(p) & = \set{p} \\
    \Sub(\bot) & = \set{\bot} \\
    \Sub(\phi_{1} \land \phi_{2}) & = \set{\phi_{1} \land \phi_{2}} \cup \Sub(\phi_{1}) \cup \Sub(\phi_{2}) \\
    \Sub(\phi_{1} \lor \phi_{2}) & = \set{\phi_{1} \lor \phi_{2}} \cup \Sub(\phi_{1}) \cup \Sub(\phi_{2}) \\
    \Sub(\phi_{1} \to \phi_{2}) & = \set{\phi_{1} \to \phi_{2}} \cup \Sub(\phi_{1}) \cup \Sub(\phi_{2})
  \end{align*}
\end{definition}

By ``iterative'' in \cref{def:pl-subformulas} we mean that formula iteration can in principle be
used to define $\Sub$.
This is not difficult but rather tedious, just in the same way it is tedious to define the
factorial function on natural numbers by iteration (see \cref{sec:nat-induction}).
But we can very easily understand how $\Sub$ operates in terms of parse trees.
Suppose $\phi$ is given by the following parse tree, in which rectangles represent again parse trees.
\begin{center}
  \begin{forest}
    for tree={%
      draw,
      circle,
      l sep=1mm,
      s sep=10mm
    }
    [$\land$
      [$\phi_{1}$, rectangle]
      [$\phi_{2}$, rectangle]
    ]
  \end{forest}
\end{center}
Then the subformulas of $\phi$ are the whole formula $\phi$ itself and the subformulas of the
formulas that are represented by the smaller parse trees.
In other words, we can list all subformulas by recursively walking through the parse tree.

\begin{example}{}{pl-subformulas}
  Let $\phi = \neg p \land q \to p \to s$.
  Then we formally obtain the subformulas of $\phi$ by
  \begin{align*}
    \Sub(\phi)
    & = \set{\phi} \cup \Sub(\neg p \land q) \cup \Sub(p \to s) \\
    & = \set{\phi} \cup \set{\neg p \land q, \neg p, p, \bot, q} \cup \set{p \to s, p, s} \\
    & = \set{\phi, \neg p \land q, \neg p, p, \bot, q, p \to s, s} \, ,
  \end{align*}
  where we use that
  \begin{align*}
    \Sub(\neg p \land q)
    & = \set{\neg p \land q} \cup \Sub(\neg p) \cup \Sub(q) \\
    & = \set{\neg p \land q} \cup \set{\neg p} \cup \Sub(p) \cup \Sub(\bot) \cup \Sub(q) \\
    & = \set{\neg p \land q} \cup \set{\neg p} \cup \set{p} \cup \set{\bot} \cup \set{q} \\
    & = \set{\neg p \land q, \neg p, p, \bot, q} \, .
  \end{align*}
  If we look at the parse tree of $\phi$, then the subformulas are evident because
  every node contributes as subformula the formula that is represents.
  \begin{center}
    \begin{forest}
      for tree={%
        draw,
        circle,
        l sep=1mm,
        s sep=10mm
      }
      [$\to$
        [$\land$
          [$\to$ [$p$] [$\bot$] ]
          [$q$]
        ]
        [$\to$
          [$p$]
          [$s$]
        ]
      ]
    \end{forest}
  \end{center}
  This gives us the following list of distinct subformulas by traversing the tree depth-first from
  left to right.
  \begin{align*}
    & \neg p \land q \to p \to s
    & & p \to s & & s\\
    & \neg p \land q
    & & q \\
    & \neg p
    & & \bot \\
    & p
  \end{align*}
\end{example}

\begin{dialogue}
  \spC These are a lot of formalities for expressing very simple things!
  Is all of this really necessary?
  \speak{Aristotle}
  If you know how computers work, then you should be able to appreciate the clarity of representing
  formulas as parse trees and formula induction as recursive visits of trees already.
  A more elaborate answer would be that we have seen throughout history everything from
  misunderstandings to lies because assertions have been misunderstood, wrong or implicit
  assumptions have been made etc.
  The formal language of propositional logic does away with all of this, as there is no ambiguity
  in assertions being made.
  If someone makes a mistake, then you can find that by inspecting the claimed formula.
  \speak{Isaac} Just by looking at the formula?
  What if I don't know what the propositional variables should mean or how one part of a formula
  relates to another?
  \speak{Aristotle} Very well observed, my little brass friend!
  \speak{Isaac} Brass friend???
  \speak{Aristotle} To get a proper answer to your question, I will have to refer you to another
  logician, one who studied the meaning of formulas rigorously.
\end{dialogue}

\dobib{}

\doappendix{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: