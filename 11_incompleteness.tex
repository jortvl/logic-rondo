\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{11}{Incompleteness and Undecidability}{fol-incompleteness}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\textcolor{red}{\textbf{Note: Incomplete lecture notes with little explanation.
    Please refer for this chapter to the lecture.}}

The following definition refers to ``computation procedures'', which we do not define precisely
here.
Think of them as a computer programs, a sequence of instructions that can be unambiguously run
by a machine.
A more precise definition can be given, for example, in terms of Turing machines or
by any other equivalent notion of computation.

\begin{definition}{}{computable}
  We define the set $\pN$ of \notion{partial numbers} by $\pN = \N \cup \set{\dc}$, where the
  symbol $\dc$ can be understood as \notion{diverging computation} or non-halting computation.
  A map $f \from \N^{n} \to \pN$ is called an (n-ary) \notion{partial function}
  or just \notion{function}.
  The \notion{domain} of $f$ is given by the set all of all inputs on which $f$ converges:
  \inlinedefbox{$\dom(f) = \setDef{x \in \N^{n}}{f(x) \ne \dc}$}.
  If $\dom(f) = \N^{n}$, then $f$ is called \notion{total}.

  We say that a function $f$ is \notion{computable} if there is a computation procedure
  that halts on all inputs $x \in \dom(f)$ with output $f(x)$, and does not halt if $f(x) = \dc$.
  A set $P \subset \N^{n}$ is \notion{semi-decidable} (or \notion{recursively enumerable}, r.e.)
  if there is a computable function $f$ with $\dom(f) = P$.
  Such a set $P$ is \notion{decidable}, if the characteristic function $\chi_{P}$ with
  \begin{equation*}
    \chi_{P}(x) =
    \begin{cases}
      0, & x \in P \\
      1, & x \not\in P
    \end{cases}
  \end{equation*}
  is total computable.
\end{definition}

The name ``recursively enumerable'' for semi-decidable sets comes from the fact that a set
$P \subseteq \N$ is semi-decidable if and only if $P$ is empty or there is a total computable
function $f \from \N \to \N$ with $P = \setDef{f(n)}{n \in \N}$.
Thus, there is a way to \emph{enumerate} the elements of $P$ as $f(0), f(1), \dotsc$

\begin{theorem}{}{fol-undecidable}
  The set $\setDef{\phi}{\seq{}{\phi} \text{ derivable in } \mathcal{D}}$ is for
  $\mathcal{D} \in $ \{\NDF{}, \cNDF{}, \NDFe{}, \cNDFe{}\} semi-decidable but not decidable.
\end{theorem}

\begin{definition}{}{closed-formulas}
  A term $t$ or formula $\phi$ is \notion{closed}, if $\var(t) = \emptyset$ or
  $\fv(\phi) = \emptyset$, respectively.
  % If $\fv(\phi) = \set{x_{1}, \dotsc, x_{n}}$ and $t_{1}, \dotsc, t_{n}$ are terms, we write
  % $\phi(t_{1}, \dotsc, t_{n})$ for $\appS{\phi}{\update{x_{1}}{t_{1}} \dotsm \update{x_{n}}{t_{n}}}$.
  \todo{Never used!}
\end{definition}

\begin{definition}{}{axiomatisable}
  A set $\Gamma$ of closed formulas is a \notion{theory}, if it is closed under deduction:
  If $\phi$ is closed and $\seq{\Gamma}{\phi}$ in \cNDFe{}, then $\phi \in \Gamma$.
  We call $\Gamma$ \notion{axiomatisable} if there is a r.e. set $\Gamma_{0} \subseteq \Gamma$,
  such that $\Gamma = \setDef{\phi}{\seq{\Gamma_{0}}{\phi} \text{ in \cNDFe{}}}$.
  The elements of $\Gamma_{0}$ are called \notion{axioms} and $\Gamma_{0}$ an
  \notion{axiomatisation}.
\end{definition}

\begin{example}{}{succ-nonzero-axiom}
  $\Gamma_{0} = \set{\all{x} \neg(s \, x \lequal \zero)}$
  axiomatises that zero is not the successor of any number.
\end{example}

\begin{definition}{}{theory-containment}
  We shall write \inlinedefbox{$\Lang(\Gamma)$} for the signature containing all symbols in
  $\Gamma$.
  Let $\Gamma$ and $\Gamma'$ be axiomatised by $\Gamma_{0}$ and $\Gamma'_{0}$, respectively.
  $\Gamma'$ contains $\Gamma$, if $\Lang(\Gamma) \subseteq \Lang(\Gamma')$\todo{Undefined} and
  $\Gamma_{0} \subseteq \Gamma'_{0}$.
\end{definition}

\begin{definition}{}{primitive-recursion}
  The set $\PR$ of \notion{primitive recursive} (p.r.) functions is the smallest set containing the
  following functions.
  \begin{itemize}
  \item Constants: $\K[n] \from \N^{n} \to \N$
    with $\K[n](x) = 0$
  \item Projection: $\P[n]{k} \from \N^{n} \to \N$
    with $\P[n]{k}(x_{1}, \dotsc, x_{n}) = x_{k}$ for $1 \leq k \leq n$
  \item Successor: $\Suc \from \N \to \N$
    with $\Suc(x) = x + 1$
  \item Composition: $\C[n][m](g, h_{1}, \dotsc, h_{n}) \from \N^{m} \to \N$
    for all p.r. functions $g \from \N^{n} \to \N$
    and $h_{1}, \dotsc, h_{n} \from \N^{m} \to \N$
    defined by
    \begin{equation*}
      \C[n][m](g, h_{1}, \dotsc, h_{n})(x) = g(h_{1}(x), \dotsc, h_{n}(x))
    \end{equation*}
  \item Iteration: $\I[n](g, h) \from \N^{n + 1} \to \N$
    for all p.r. functions $g \from \N^{n} \to \N$
    and $h \from \N^{n + 2} \to \N$
    defined by
    \begin{align*}
      \I[n](g, h)(0, x) & = g(x) \\
      \I[n](g, h)(x + 1, y) & = h(\I[n](g, h)(x, y), x, y)
    \end{align*}
  \end{itemize}
\end{definition}

\begin{example}{}{pred-addition-pr}
  \begin{enumerate}
  \item
    The cut-off predecessor function $\pred \from \N \to \N$, specified by
    \begin{equation*}
      \pred(n) =
      \begin{cases}
        0, & n = 0 \\
        n - 1, & n > 0
      \end{cases} \, ,
    \end{equation*}
    is primitive recursive,
    as we have that $\pred = \I[1](\K[0], \P[2]{2})$.
    That this identity holds can be seen by evaluating the right-hand side as
    far as possible and match it with the specification of $\pred$.
    Since the function is given by iteration, we distinguish the base case and
    the step case:
    \begin{align*}
      \I[1](\K[0], \P[2]{2})(n)
      & =
      \begin{cases}
        \K[0](), & n = 0 \\
        \P[2]{2}(\I[1](\K[0], \P[2]{2})(k), k), & k = n + 1
      \end{cases}
      \\
      & =
      \begin{cases}
        0, & n = 0 \\
        k, & n = k + 1
      \end{cases}
      \\
      & =
      \begin{cases}
        0, & n = 0 \\
        n - 1, & n > 0
      \end{cases}
    \end{align*}
    This corresponds to our specification of the predecessor.

  \item Note that the predecessor is defined by case distinction on its argument.
    We can use the same idea to show that case distinction is primitive recursive in general.
    Given $f \from \N^{n} \to \N$ and $g \from \N^{n+1} \to \N$, the function
    $[f,g] \from \N^{n + 1} \to \N$, given by
    \begin{equation*}
      [f,g](n, x)
      =
      \begin{cases}
        f(x), & n = 0 \\
        g(k, x), & n = k + 1
      \end{cases}
      \, ,
    \end{equation*}
    is primitive recursive because
    \begin{equation*}
      [f,g] = \I[n](f, \C(g, \P[n+2]{2}, \dotsc, \P[n+2]{n+2})) \, .
    \end{equation*}

  \item Neither of the two examples above has used proper recursion, as both ignored
    the recursion parameter in their use of iteration.
    To show that addition is primitive recursive, we observe that it obeys two recursive
    equations: $0 + m = m$ and $(n + 1) + m = (n + m) + 1$.
    We can emulate these with iteration and define $p \from \N^{2} \to \N$ as follows.
    \begin{equation*}
      p = \I[2](\P[1]{1}, \C[1][3](S, \P[3]{1}))
    \end{equation*}
    With this definition we have for all $n, m \in \N$ that
    \begin{equation*}
      p(0, m) = \P[1]{1}(m) = m
    \end{equation*}
    and
    \begin{align*}
      p(n + 1, m)
      & = \C[1][3](S, \P[3]{1})(p(n, m), n, m) \\
      & = S(\P[3]{1}(p(n, m), n, m)) \\
      & = S(p(n, m)) \\
      & = p(n, m) + 1
      \, .
    \end{align*}
    Therefore, $p$ fulfils the same recursive equations as $+$, which one can use to
    prove that $p(n,m) = n + m$.
  \end{enumerate}
\end{example}

\textcolor{red}{\textbf{Note: Remainder of lecture missing!}}


\dobib{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: