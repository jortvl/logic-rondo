\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{7}{Proof Theory of First-Order Predicate Logic}{fol-proofs}

\section{Substitution in First-Order Logic}
\label{sec:substitution}

In these notes, we will discuss an important operation of first-order logic:
the substitution of a term for a variable.
This operation will replace any occurrence of a variable in a formula by the
given term.
For instance, we will be able to substitute the term $f(m, x)$ for the variable
$y$ in the formula $P(y) \land Q(r, y)$ to obtain
\begin{equation*}
  P(f(m, x)) \land Q(r, f(m, x)).
\end{equation*}
However, the substitution operation is surprisingly complex, as we need to deal
with variables that are bound by quantifiers.
The aim of these notes is to give a rigorous presentation of variables
and binding that allows us to safely carry out substitutions.

\subsection{The Difficulty of Names and Variables}
\label{sec:difficulty}

Let us first discuss two questions that arise in FOL:
\begin{enumerate}
\item Are the formulas $\all{x} P(x)$ and $\all{y} P(y)$ expressing the same?
\item What is the scope of variables?
\end{enumerate}

\paragraph{Formula Equality and Renaming}

The first question can be answered by reading the formula without explicitly
naming variables:
\begin{quotation}
  ``$P$ holds for all objects''.
\end{quotation}
Clearly, this sentence corresponds to both formulas $\all{x} P(x)$ and
$\all{y} P(y)$, and we should consider both formulas to be the same,
already \emph{syntactically}!
This gives us a first rule that we will have to adhere to for FOL:
\begin{defbox}
  Two formulas are considered to be the same, if we can
  \emph{bijectively rename} the variables of one formula to obtain the other.
\end{defbox}
For instance, consider the formulas $\phi$ and $\psi$ given by
$\phi = \all{x} \all{y} Q(x, y)$ and
$\psi = \all{w} \all{z} Q(w, z)$.
Then, a bijective renaming would be to rename $x$ to $w$ and $y$ to $z$,
which allows us to transform $\phi$ into $\psi$.
However, renaming $x$ to $r$ and $y$ to $r$ is not bijective.
Thus, we do not consider the formula $\all{r} \all{r} Q(r, r)$ to be
same as $\phi$.
Note, that in the latter formula, the $r$ refers to the inner-most quantifier
and the outer quantifier has no effect!

\paragraph{Scoping}

The second question concerns the scope of variables and it requires us to determine which objects a
variable refers to.
For instance, the variable $x$ in the sub-formula $P(x)$ of $\all{x} P(x)$
refers to what the quantifier ranges over.
We say that $x$ is \emph{in the scope} of $\forall x$ in this formula.
However, the variable $y$ in $\all{x} Q(x, y)$ is in the scope of no quantifier
and is thus a global reference.
If we naively substituted $x$ for $y$ in this formula, then we would
obtain $\all{x} Q(x, x)$.
Here, the scope of the second argument of $Q$, and with it the meaning of the
formula, has suddenly changed.
This leads to a second rule:
\begin{defbox}
  Substituting a term in a formula should not change variable scoping.
\end{defbox}

\subsection{De Bruijn Trees}
\label{sec:dB-trees}

There are several ways to deal with variables, binding and substitution.
An intuitively understandable way is to represent terms and formulas as
\emph{de Bruijn trees}.
The idea is that bound variables are represented by a number that points
to the quantifier that binds this variables.
All free variables keep their name.

\Cref{fig:dBTreeRep} shows the (de Bruijn-) tree representation
of $\all{x} \all{y} Q(x, f(y, z))$, where the left figure
shows the actual tree and the right figure indicates to which quantifier
the numerical name refers.
\begin{figure}
  \centering
  \begin{subfigure}{0.35\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, baseline, name=outer
      [$\forall$, name=inner
      [$Q$
      [$\dBName{1}$, name=var-outer]
      [$f$ [$\dBName{0}$, name=var-inner] [$z$]]
      ] ] ]
      \draw[-{Stealth}, draw=none] (var-inner) to[out=-35,out distance=60pt,in=-35] (inner);
    \end{forest}
    \caption{The plain tree}
    \label{fig:dBTreeRep-plain}
  \end{subfigure}
  \begin{subfigure}{0.6\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, name=outer
      [$\forall$, name=inner
      [$Q$
      [$\dBName{1}$, name=var-outer]
      [$f$ [$\dBName{0}$, name=var-inner] [$z$]]
      ] ] ]
      \draw[-{Stealth}, dotted] (var-outer) to[out=south west,in=south west] (outer);
      \draw[-{Stealth}, dotted] (var-inner) to[out=-35,out distance=60pt,in=-35] (inner);
    \end{forest}
    \caption{Tree with indication of the variable references}
    \label{fig:dBTreeRep-ref}
  \end{subfigure}
  \caption[de Bruijn-Tree Representation]{%
    de Bruijn-Tree Representations of $\all{x} \all{y} Q(x, f(y, z))$}
  \label{fig:dBTreeRep}
\end{figure}
We see that the bound variables are numbered starting from the \emph{inner-most}
quantifier.
Also note that the variable $z$ is free and thus keeps its name in the tree
representation.

Since the nesting depth of quantifiers is important, we also note that tree
representations are \emph{not closed under subtrees}!
For instance, the tree in \cref{fig:invalid-dBTree} is not a valid tree
representation because $\dBName{1}$ is a dangling reference.
\begin{figure}[htb]
  \centering
  \begin{subfigure}{0.45\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$
      [$Q$
        [$\dBName{1}$]
        [$f$ [$\dBName{0}$] [$z$]]
      ] ]
    \end{forest}
    \caption{Invalid de Bruijn-Tree}
    \label{fig:invalid-dBTree}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$
      [$Q$
        [$x$]
        [$f$ [$\dBName{0}$] [$z$]]
      ] ]
    \end{forest}
    \caption{Correct tree representing $\all{y} Q(x, f(y, z))$}
    \label{fig:dBTree-subformula}
  \end{subfigure}
  \caption{Attempts of finding trees that represent the
    subformula $\all{y} Q(x, f(y, z))$
    of $\all{x} \all{y} Q(x, f(y, z))$}
  \label{fig:dBTree-attempts}
\end{figure}
Instead, if we want to remove the outer quantifier, we need to pick a name
that we replace $\dBName{1}$ with, say $x$, and then obtain the tree
representation in \cref{fig:dBTree-subformula}.

In what follows, we will not work explicitly with tree representations, but
will use another approach, see \cref{sec:axioms} below.
However, the tree representation shows how we can solve the initial problems:
\begin{figure}[htb]
  \centering
  \begin{forest}
    for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
    [$\forall$
      [$P$ [$\dBName{0}$]
    ]]
  \end{forest}
  \caption{De Bruijn-Tree representing both formulas $\all{x} P(x)$
    and $\all{y} P(y)$}
  \label{fig:dBTree-equal}
\end{figure}
\begin{figure}
  \centering
  \begin{subfigure}{0.45\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, baseline
        [$Q$ [$\dBName{0}$] [$y$]]
      ]
    \end{forest}
    \caption{Representation before substitution}
    \label{fig:dBTree-orig}
  \end{subfigure}
  \begin{subfigure}{0.45\textwidth}
    \centering
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, baseline
        [$Q$ [$\dBName{0}$] [$x$]]
      ]
    \end{forest}
    \caption{Representation after substitution}
    \label{fig:dBTree-after}
  \end{subfigure}
  \caption{Substitution of the free variable $x$ for the free variable
    $y$ by using the tree representation of $\all{x} Q(x, y)$}
  \label{fig:dBTree-subst}
\end{figure}
\begin{enumerate}
\item The formulas $\all{x} P(x)$ and $\all{y} P(y)$ have the same
  tree representation, see \cref{fig:dBTree-equal}.
\item Substitutions can be carried out without changing the binding
  of variables, see \cref{fig:dBTree-subst}.
  Note that the \emph{bound} variable $x$ is represented by $\dBName{0}$ and
  thus there is no danger of substituting the \emph{unbound} variable $x$
  for the \emph{unbound} variable $y$.
  Hence, the problem of accidentally binding a variable does not exist for
  the tree representation.
\end{enumerate}

\subsection{Axiomatising Terms and Substitutions}
\label{sec:axioms}

The tree representations of formulas that we saw in \cref{sec:difficulty}
solves our problems but it is difficult to work with.
In fact, these trees are good way of \emph{implementing} FOL on a computer,
but not for humans to work with.
The aim of this section is to introduce a bunch of axioms that formulas and
substitution have to fulfil.
These axioms are fulfilled if we were to represent terms by de Bruijn trees,
but for the remainder of the course, we will leave unspecified how formulas
are implemented.

We begin with the definition of substitutions and how they can be applied to
terms.
\begin{definition}{}{def:subst}
  A \termdef{substitution} is a map $\sigma \from \Var \to \Term$.
  Given $t \in \Term$ and $x \in \Var$, we write
  $\sigma \update{x}{t}$ for the \termdef{updated substitution} defined by
  \begin{emphdef}{equation*}
    (\sigma \update{x}{t})(y) =
    \begin{cases}
      t, & x = y \\
      \sigma(y), & x \neq y
    \end{cases}
  \end{emphdef}
  and $\eta \from \Var \to \Term$ for the \termdef{identity substitution} given by
  $\eta(y) = y$.
  For convenience, we write $\update{x}{t}$ as shorthand for $\eta \update{x}{t}$.
  Given a term $t$, we write $\appS{t}{\sigma}$ for the \termdef{application}
  of $t$ to the substitution $\sigma$, defined by iteration as follows.
  \begin{emphdef}{align*}
    \appS{x}{\sigma} & = \sigma(x) \\
    \appS{c}{\sigma} & = c \\
    \appS{f(t_1, \dotsc, t_n)}{\sigma} & = f(\appS{t_1}{\sigma}, \dotsc, \appS{t_n}{\sigma})
  \end{emphdef}
\end{definition}

The notation $\sigma\update{x}{t}$ to update a substitution $\sigma$ can be read like an assignment
in an imperative programming language: the term that $\sigma$ assigned to $x$ will be overwritten by
$t$.
Similarly, the notation $\update{x}{t}$ starts with a storage in which all variables $y$ have the
default value $y$, except for $x$ which gets $t$ assigned as initial value.

\begin{quiz}
  Can you make sense of the name \emph{identity substitution}?
  What is the result of applying a term to $\eta$?
\end{quiz}
\begin{answer}
  If $t$ is any term, then it is easy to see that $\appS{t}{\eta} = t$.
  Thus, the identity substitution is the identity for the application of terms to substitutions.
  Formally, this can be proven by induction over terms.

  In fact, this goes even deeper.
  We can compose substitutions, just like we compose maps:
  For substitutions $\sigma$ and $\tau$, let us denote by $\tau \odot \sigma$ their composition,
  which is given by applying to every term $\sigma(x)$ the substitution $\tau$:
  \begin{equation*}
    (\tau \odot \sigma)(x) = \appS{\sigma(x)}{\tau}
  \end{equation*}
  Since $\appS{\sigma(x)}{\eta} = \sigma(x)$, we thus obtain $\eta \odot \sigma = \sigma$.
  The other way round, we also have $(\sigma \odot \eta)(x) = x \sigma = \sigma(x)$.
  Hence, composition with $\eta$ does not change a substitution and thus the identity substitution
  acts like the identity map.
\end{answer}

The following example illustrates these definitions.
\begin{example}{}{}
  Let $g(x,y)$ be a term with two free variables $x$ and $y$, and one binary function symbol $g$.
  Moreover, let $\sigma = \update{x}{y}\update{y}{x}$.
  \begin{enumerate}
  \item The substitution $\sigma$ exchanges the two variables:
    \begin{equation*}
      \appS{g(x,y)}{\sigma}
      = g(\appS{x}{\sigma}, \appS{y}{\sigma})
      = g(\sigma(x), \sigma(y))
      = g(y, x)
    \end{equation*}

    This example shows that the substitution $\update{x}{y}\update{y}{x}$ is \emph{not} the same
    as $\eta$.
    In fact, $\sigma$ is substituting for $x$ and $y$ \emph{in parallel} and not in sequence.
    Therefore, we also cannot obtain $\sigma$ by repeated application:
    \begin{equation*}
      \appS{g(x, y)}{\update{x}{y}\update{y}{x}}
      \neq \appS{(\appS{g(x, y)}{\update{x}{y}})}{\update{y}{x}}
      = g(x, x)
    \end{equation*}
  \item Let now $\tau = \sigma\update{y}{f(x)}$.
    In $\tau$, the assignment of $x$ to $y$ is overwritten with the assignment of $f(x)$ to $y$.
    We thus have the following.
    \begin{equation*}
      \appS{g(x,y)}{\tau}
      = g(\appS{x}{\tau}, \appS{y}{\tau})
      = g(y, f(x))
    \end{equation*}
  \end{enumerate}
\end{example}

Next, we need to be able to apply substitutions to formulas.
To circumvent the difficulties described in \cref{sec:difficulty}, we
\emph{assume} that there is a set of terms on which we can carry out
substitutions.
This set of terms can \emph{in principle} be defined by appealing to the
tree representation.
However, this is tedious and instead we just assume that formulas and
the application of substitutions fulfil certain \emph{axioms}.
\begin{definition}{Extension of \cref{def:fol-formulas-approx}}{formula-axioms}
  Given a formula $\phi$, a variable $x$ and a substitution $\sigma$, we say
  that $x$ is \termdisp{fresh for formula}{fresh for $\phi$},
  written \inlinedefbox{$\fresh{x}{\phi}$}, if $x \not\in \fv(\phi)$.
  Similarly, we say that $x$ is \termdisp{fresh for substitution}{fresh for $\sigma$},
  written \inlinedefbox{$\fresh{x}{\sigma}$}, if
  \begin{equation*}
    x \not\in \var(\sigma(y)) \text{ for all }
    y \in \Var \smallsetminus \set{x} \, .
  \end{equation*}

  Additionally to the closure rules in \cref{def:fol-formulas-approx}, we assume that
  we can \termdisp{application of formula}{apply a FOL formula $\phi$ to a substitution $\sigma$},
  written $\appS{\phi}{\sigma}$.
  Further, we assume for all $\lozenge \in \set{\forall, \exists}$ and
  $\Box \in \set{\land, \lor, \to}$ that the equality on formulas
  fulfils the following six axioms.
  \begin{emphaxioms}{align*}
    % P(t_1, \dotsc, t_n) = P'(t_1', \dotsc, t_n')
    % & \text{ iff }
    % P = P' \text{ and } t_k = t_k' \text{ for all } 1 \leq k \leq n
    % \tag{EP} \label{ax:EP} \\
    \phi \BoxOp \psi = \phi' \BoxOp \psi'
    & \text{ iff }
    \phi = \phi' \text{ and } \psi = \psi'
    \tag{EC} \label{ax:EC} \\
    \quant{x} \phi = \quant{y} \psi
    & \text{ iff }
    \fresh{y}{\phi} \text{ and } \psi = \phi\update{x}{y}
    \tag{EQ} \label{ax:EQ} \\
    \appS{\bot}{\sigma}
    & = \bot
    \tag{SB} \label{ax:SB} \\
    \appS{P(t_1, \dotsc, t_n)}{\sigma}
    & = P(\appS{t_1}{\sigma}, \dotsc, \appS{t_n}{\sigma})
    \tag{SP} \label{ax:SP} \\
    \appS{(\phi \BoxOp \psi)}{\sigma}
    & = \appS{\phi}{\sigma} \BoxOp \appS{\psi}{\sigma}
    \tag{SC} \label{ax:SC} \\
    \appS{(\quant{x} \phi)}{\sigma}
    & = \quant{x} \appS{\phi}{\sigma \update{x}{x}}
    \text{ if } \fresh{x}{\sigma}
    \tag{SQ} \label{ax:SQ}
  \end{emphaxioms}
\end{definition}

\begin{quiz}
  Do you know why we need to exclude in the definition of $\fresh{x}{\sigma}$ the variable $x$
  from the quantification?
\end{quiz}
\begin{answer}
  The reason is that in the axiom \axref{SP}, we would otherwise exclude even the identity
  substitution because $x \in \var(\eta(x))$.
  However, the present definition gives us $\fresh{x}{\eta}$
  and we could prove $\appS{\phi}{\eta} = \phi$.
\end{answer}

Let us explain the intuition behind these axioms.
First of all, there are two groups of axioms: those for the equality on
formulas (starting with E) and those that define the action of substitution on
formulas (starting with S).
The axiom \axref{EQ} allows us to bijectively rename bound variables
without illegally binding other variables.
For instance, we have
\begin{equation*}
  \all{x} Q(x, y) = \all{z} Q(z, y)
\end{equation*}
because $z$ is fresh for $Q(x,y)$, that is $z \not\in \fv(Q(x,y))$.
However, we have
\begin{equation*}
  \all{x} Q(x, y) \ne \all{y} Q(y, y)
\end{equation*}
because $y \in \fv(Q(x,y))$.
The axiom \axref{EC} allows us to carry out this renaming also in complex
formulas that involve other connectives than quantifiers.
Implicitly, we also use equality on terms and atoms, in the sense that
\begin{equation*}
  P(t_1, \dotsc, t_n) = P'(t_1', \dotsc, t_n')
  \text{ iff }
  P = P' \text{ and } t_k = t_k' \text{ for all } 1 \leq k \leq n
\end{equation*}
and that equality is an equivalence relation (reflexive, symmetric
and transitive).

The second group of axioms describes how substitution can be computed
iteratively.
Axioms \axref{SB}, \axref{SP} and \axref{SC} are doing what we would expect:
no action on the atom $\bot$, reduce subsitution on predicates to substitution
in terms, and distribute substitution over propositional connectives.
Complications arise only in the axiom \axref{SQ}, which has to make sure that
the \emph{use of a bound variable is not changed} and
that \emph{variables are not accidentally bound}.
On the one hand, that the use of a bound variable is not changed is achieved by
updating the substitution $\sigma$ to $\sigma \update{x}{x}$.
For instance, if $\sigma(x) = g(y)$, then naively carrying out this substitution
on $\all{x} P(x)$ would lead to $\all{x} P(g(y))$, which is certainly not what
we want!
Instead, we have by \axref{SQ} and \axref{SP}
\begin{equation*}
  \appS{(\all{x} P(x))}{\sigma} = \all{x} \appS{P(x)}{\sigma \update{x}{x}} = \all{x} P(x).
\end{equation*}
Accidental binding is prevented, on the other hand, by the precondition that
$x$ must be fresh for $\sigma$.
This condition ensures that none of the terms we want to substitute for the
(free) variables in $\phi$ contains the variable $x$, which would become then
bound by the quantifier.

These rules and their interaction are best illustrated through some examples.
\begin{example}{}{formula-subst}
  In the following, we use the substitution $\sigma$ given by
  \begin{equation*}
    \sigma = \update{y}{x}\update{z}{m}.
  \end{equation*}
  \begin{enumerate}
  \item Let $\phi = \all{z} Q(z,y)$.
    First, we note that $\var(\sigma(y)) = \set{x}$ and
    thus $z \not\in \fv(\sigma(v))$ for all $v \neq z$.
    Thus, $\fresh{z}{\sigma}$.
    This allows us to carry out the substitution:
    \begin{align*}
      \appS{\phi}{\sigma}
      & = \all{z} \appS{Q(z,y)}{\sigma \update{z}{z}}
      \tag{SQ} \\
      & = \all{z} Q(\appS{z}{\sigma \update{z}{z}},\appS{y}{\sigma \update{z}{z}})
      \tag{SP} \\
      & = \all{z} Q(z,x)
    \end{align*}
    Note that $\sigma \update{z}{z} = \update{y}{x}$
    and the substitution of $m$ for $z$ in $\sigma$ was ``forgotten''
    when we applied the substitution under the quantifier.
    This is intuitively expected, as the bound variable $z$ in $\phi$
    is a local reference, while the $z$ in $\sigma$ refers to a global
    variable $z$ that has the same name but is distinct from the local
    variable.
  \item Let $\psi = \all{x} Q(x, y)$.
    First, we note that $\var(\sigma(y)) = \set{x}$.
    Thus, $x \in \fv(\sigma(y))$ and $x$ is \emph{not} fresh.
    However, we have $\fresh{z}{Q(x,y)}$ and
    $Q(x,y)\update{x}{z} = Q(z,y)$.
    This allows us to rename the bound variable $x$ in $\psi$ to $z$
    and then carry out the substitution as above:
    \begin{align*}
      \appS{(\all{x} Q(x, y))}{\sigma}
      & = \appS{(\all{z} Q(z,y))}{\sigma}
      \tag{EQ} \\
      & = \all{z} Q(z, x)
      \tag*{by 1.}
    \end{align*}
    Note that we cannot safely rename $z$ back to $x$, as we would otherwise
    illegally bind $x$.
  \end{enumerate}
\end{example}

In the remainder, we will not make explicit use of the axioms
provided in \cref{def:formula-axioms}.
Instead, we will rename \emph{bound} variables, if necessary, before
carrying out substitutions.
For instance, we would just write
\begin{equation*}
  \appS{(\all{x} Q(x, y))}{\update{y}{x}} = \all{z} Q(z, x)
\end{equation*}
without explicitly referring to the axioms.
However, we know that in cause of doubt, we can always go back to the axioms
and formally carry out the renaming and substitution.

Our equality axioms for formulas and the possibility of renaming variables whenever necessary has as
consequence that bound variables are \emph{internal} to formulas and cannot be observed.
The supposed map $\bv$ that extracts bound variables from a formula that we introduced
in \cref{def:fol-fv-bv} is in fact not well-defined for formulas that fulfil the axioms of
\cref{def:formula-axioms}.
For instance, the formulas $\all{x} P(x)$ and $\all{y} P(y)$ are equal according to the axioms but
\begin{equation*}
  \bv(\all{x} P(x)) = \set{x} \ne \set{y} = \bv(\all{y} P(y))
\end{equation*}
and thus $\bv$ cannot be a map.
In general, whenever we want to define a map on formulas, then such a map has to respect the
equality and thus be invariant under renaming.
More precisely, if $f \from \Forms(\Lang) \to A$ is a map and $\phi = \psi$, according to
\cref{def:formula-axioms}, then $f(\phi) = f(\psi)$.


\section{Natural Deduction for FOL}
\label{sec:nd-fol}

\textcolor{red}{\textbf{Note: This section has no explanation. Please refer to the lectures.}}

Similarly to propositional logic, we want \emph{syntactic proofs} for FOL.

\subsection{The Intuitionistic System \NDF{}}
\label{sec:int-nd-fol}

As we reason about objects in FOL, we need a new definition of sequents:
\begin{definition}{}{fol-sequent}
  A \termdef{fo sequent} for a signature $\Lang$ is a triple
  \begin{equation*}
    \seq[\Delta]{\Gamma}{\phi} \, ,
  \end{equation*}
  where
  $\Delta$ is a list of variables in $\Var$,
  $\Gamma$ is a list of $\Lang$-formulas,
  $\phi$ is a $\Lang$-formula, and
  $\fv(\Gamma) \cup \fv(\phi) \subseteq \abs{\Delta}$ for
  $\abs{\Delta} = \setDef{x \in \Var}{x : \Delta}$.
  If $\Delta$ is the empty list $\cdot$, we write
  \begin{equation*}
    \seq{\Gamma}{\phi}
  \end{equation*}
  instead of $\seq[\cdot]{\Gamma}{\phi}$.
\end{definition}

\begin{example}{}{fol-sequents}
  \begin{itemize}
  \item This is a sequent:
    \begin{equation*}
      \seq[x,y]{\all{z} Q(z,y)}{P(x)}
    \end{equation*}
    because $\fv(\all{z} Q(z,y)) \cup \fv(P(x)) = \set{x,y}$.

  \item This is a sequent:
    \begin{equation*}
      \seq[x,y,z]{\all{z} Q(z,y)}{P(x)}
    \end{equation*}
    because $\fv(\all{z} Q(z,y)) \cup \fv(P(x)) = \set{x,y} \subseteq \set{x,y,z}$.

  \item This is not a sequent:
    \begin{equation*}
      \seq[x]{\all{z} Q(z,y)}{P(x)}
    \end{equation*}
    because $\fv(\all{z} Q(z,y)) \cup \fv(P(x)) \not\subseteq \set{x}$.
  \end{itemize}
\end{example}

\

\todo[inline]{Add table with intuition for quantifier rules}

\begin{figure}[ht]
  \centering
  \begin{gather*}
    {\begin{prooftree}
        \hypo{\phi : \Gamma}
        \infer1[\Ax]{\seq[\Delta]{\Gamma}{\phi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\bot}}
        \infer1[\EBot]{\seq[\Delta]{\Gamma}{\phi}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi \land \psi}}
        \infer1[\LEAnd]{\seq[\Delta]{\Gamma}{\phi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi \land \psi}}
        \infer1[\REAnd]{\seq[\Delta]{\Gamma}{\psi}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi}}
        \hypo{\seq[\Delta]{\Gamma}{\psi}}
        \infer2[\IAnd]{\seq[\Delta]{\Gamma}{\phi \land \psi}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi}}
        \infer1[\LIOr]{\seq[\Delta]{\Gamma}{\phi \lor \psi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\psi}}
        \infer1[\RIOr]{\seq[\Delta]{\Gamma}{\phi \lor \psi}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi \lor \psi}}
        \hypo{\seq[\Delta]{\Gamma,\phi}{\delta}}
        \hypo{\seq[\Delta]{\Gamma,\psi}{\delta}}
        \infer3[\EOr]{\seq[\Delta]{\Gamma}{\delta}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma, \phi}{\psi}}
        \infer1[\IImpl]{\seq[\Delta]{\Gamma}{\phi \to \psi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\phi \to \psi}}
        \hypo{\seq[\Delta]{\Gamma}{\phi}}
        \infer2[\EImpl]{\seq[\Delta]{\Gamma}{\psi}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta,x]{\Gamma}{\phi}}
        \infer[left label={($x \not\in \Delta$)}]1[\IAll]{\seq[\Delta]{\Gamma}{\all{x} \phi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\all{x} \phi}}
        \infer1[\EAll]{\seq[\Delta]{\Gamma}{\appS{\phi}{\update{x}{t}}}}
      \end{prooftree}}
    \\[7pt]
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\appS{\phi}{\update{x}{t}}}}
        \infer1[\IEx]{\seq[\Delta]{\Gamma}{\exist{x} \phi}}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma}{\exist{x} \phi}}
        \hypo{\seq[\Delta,x]{\Gamma,\phi}{\psi}}
        \infer[left label={($x \not\in \Delta$)}]2[\EEx]{\seq[\Delta]{\Gamma}{\psi}}
      \end{prooftree}}
  \end{gather*}
  \caption{Deduction Rules of the natural deduction system \NDF{}}
  \label{fig:nd-fol-rules}
\end{figure}

\begin{definition}{Intuitionistic natural deduction for FOL}{fol-nd}
  The \termdef{system ND} for FOL is given by the rules in \cref{fig:nd-fol-rules},
  where the label $x \not\in \Delta$ in the rules \IAll{} and \EEx{} are side-conditions
  that have to be fulfilled to apply those rules.\todo{Define derivability}
  However, these side-condition will not be displayed in proof trees.
\end{definition}

Note about \cref{fig:nd-fol-rules}:
\begin{itemize}
\item In \EAll{} and \IEx{}, the definition of a sequent ensures in
  $\seq[\Delta]{\Gamma}{\phi \update{x}{t}}$ that all the free variables of $t$ appear in $\Delta$.
\item Similarly, it is ensured in \EEx{} that the variable $x$ does not occur freely in $\psi$
  because $\seq[\Delta]{\Gamma}{\psi}$ is a sequent.
\end{itemize}

\subsection{Fitch-Style Deduction for \NDF{}}
\label{sec:fitch-nd-fol}

\begin{example}{}{exist-constant}
  We prove
  $\seq{\all{x}P(c, x)}{\all{x} \exist{y} P(y, x)}$, where $c$ is a constant
  \begin{equation*}
    \begin{nd}
      \hypo {h} {\all{x}P(c, x)}
      \open[x]
      \hypo {xi} {}
      \have {c}  {P(c, x)}                   \Ae{h}
      \have {gA} {\exist{y} P(y, x)}         \Ei{c}
      \close
      \have {r} {\all{x} \exist{y} P(y, x)}   \Ai{xi-gA}
    \end{nd}
  \end{equation*}
\end{example}

\begin{example}{}{ex-to-neg-all}
  We prove
  $\seq[y]{\exist{x}P(x, y)}{\neg (\all{x} \neg P(x, y))}$
  \begin{equation*}
    \begin{nd}
      \open[y]
      \hypo {h} {\exist{x}P(x, y)}
      \open[x]
      \hypo {eh} {P(x,y)}
      \open
      \hypo {nh} {\all{x} \neg P(x, y)}
      \have {nP} {\neg P(x, y)}                  \Ae{nh}
      \have {ng} {\bot}                          \ne{nP,eh}
      \close
      \have {ge} {\neg (\all{x} \neg P(x, y))}   \ni{nh-ng}
      \close
      \have {r} {\neg (\all{x} \neg P(x, y))}    \Ee{h, eh-ge}
      \close
    \end{nd}
  \end{equation*}
\end{example}


\begin{example}{}{neg-ex-all-neg}
  We prove $\seq{\neg \exist{x} \phi}{\all{x} \neg \phi}$.
  \begin{equation*}
    \begin{nd}
      \hypo {h} {\neg \exist{x} \phi}
      \open[x]
      \have {xi} {}
      \open
      \hypo {hN} {\phi}
      \have {e}  {\exist{x} \phi}     \Ei{hN}
      \have {gN} {\bot}               \ne{h,e}
      \close
      \have {gA} {\neg \phi}          \ni{hN-gN}
      \close
      \have {r} {\all{x} \neg \phi}   \Ai{xi-gA}
    \end{nd}
  \end{equation*}
\end{example}

\subsection{The Classical System \cNDF{}}
\label{sec:classical-nd-fol}

\begin{definition}{}{fol-cnd}
  The \termdef{system cND} of natural deduction for \emph{classical} first-order logic is given by
  the system \NDF{} together with the contradiction rule:
  \begin{equation*}
    {\begin{prooftree}
        \hypo{\seq[\Delta]{\Gamma, \neg \phi}{\bot}}
        \infer1[\Contra]{\seq[\Delta]{\Gamma}{\phi}}
      \end{prooftree}}
  \end{equation*}
\end{definition}

\todo[inline]{Fill in}

\section{Exercises}
\label{sec:exercises}

The following exercises allow you to practise the material of this chapter.

\begin{exercise}
  Give the de Bruijn-tree representations of the following formulas.
  \begin{tasks}(2)
    \task $Q(x, g(m))$
    \task $\all{x} Q(x, g(m))$
    \task $(\exist{x} Q(x, g(m))) \land \all{y} Q(y, z)$
    \task $\exist{y} Q(y, g(z)) \land \all{x} Q(y, f(x, z))$
    \task $\all{x} \exist{x} P(x)$
  \end{tasks}
\end{exercise}
\begin{solution}
  \begin{tasks}(2)
    \task
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$Q$, baseline [$x$] [$g$ [$m$]]]
    \end{forest}
    \task
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, baseline
        [$Q$ [$\dBName{0}$] [$g$ [$m$]]
      ]]
    \end{forest}
    \task
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\land$, baseline
      [$\exists$
        [$Q$ [$\dBName{0}$] [$g$ [$m$]]]
      ]
      [$\forall$
        [$Q$ [$\dBName{0}$] [$z$]]
      ]
      ]
    \end{forest}
    \task
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\exists$, baseline
      [$\land$
        [$Q$ [$\dBName{0}$] [$g$ [$z$]]]
        [$\forall$
          [$Q$ [$\dBName{1}$] [$f$ [$\dBName{0}$] [$z$]]]
        ]
      ]]
    \end{forest}
    \task
    \begin{forest}
      for tree={inner sep=2pt,l'=10pt,l sep'=8pt,s'=10pt,s sep'=15pt}
      [$\forall$, baseline [$\exists$ [$P$ [$\dBName{0}$]]]]
    \end{forest}
  \end{tasks}
\end{solution}

\begin{exercise}
  Let $\sigma$ be given by
  \begin{equation*}
    \sigma = \update{x}{g(x)} \update{y}{x} \update{z}{y}.
  \end{equation*}
  Evaluate the following applications of terms to $\sigma$.
  \begin{tasks}(4)
    \task $\appS{x}{\sigma}$
    \task $\appS{f(x, y)}{\sigma}$
    \task $\appS{(\appS{y}{\sigma})}{\sigma}$
    \task $\appS{x}{(\sigma\update{x}{x})}$
  \end{tasks}
\end{exercise}
\begin{solution}
  \begin{tasks}
    \task $\appS{x}{\sigma} = \sigma(x) = g(x)$
    \task $\appS{f(x, y)}{\sigma} = f(\sigma(x), \sigma(y)) = f(g(x), x)$
    \task $\appS{(\appS{y}{\sigma})}{\sigma} = \appS{x}{\sigma} = g(x)$
    \task $\appS{x}{(\sigma\update{x}{x})} = (\sigma\update{x}{x})(x) = x$
  \end{tasks}
\end{solution}

\begin{exercise}
  Let $\sigma$ be given by
  \begin{equation*}
    \sigma = \update{x}{g(x)} \update{y}{x} \update{z}{y}
  \end{equation*}
  and $\phi$ by
  \begin{equation*}
    \phi = \all{x} Q(x, y,z).
  \end{equation*}
  Determine which of the following relations hold.
  \begin{tasks}(3)
    \task $\fresh{x}{\sigma}$
    \task $\fresh{y}{\sigma}$
    \task $\fresh{z}{\sigma}$
    \task $\fresh{x}{\phi}$
    \task $\fresh{y}{\phi}$
    \task $\fresh{z}{\phi}$
  \end{tasks}
\end{exercise}
\begin{solution}
  We have $\fv(\phi) = \set{y,z}$,
  $\var(\sigma(x)) = \set{x}$,
  $\var(\sigma(y)) = \set{x}$,
  $\var(\sigma(z)) = \set{y}$,
  and $\var(\sigma(v)) = \set{v}$ for all other variables $v$.
  This gives us
  \begin{tasks}
    \task $x$ is not fresh for $\sigma$, since $x \in \var(\sigma(y))$.
    \task $y$ is not fresh for $\sigma$, since $y \in \var(\sigma(z))$.
    \task $z$ is fresh for $\sigma$, since $z \not\in \var(\sigma(v))$
    for any variable $v$.
    \task $x$ is fresh for $\phi$, since $x \not\in \fv(\phi)$.
    \task $y$ is not fresh for $\phi$, since $y \in \fv(\phi)$.
    \task $z$ is not fresh for $\phi$, since $z \in \fv(\phi)$.
  \end{tasks}
\end{solution}

\begin{exercise}
  Let $\sigma$ be given by
  \begin{equation*}
    \sigma = \update{x}{g(x)} \update{y}{x} \update{z}{y}.
  \end{equation*}
  Carry out the following substitutions.
  \begin{tasks}(2)
    \task $\appS{Q(x, g(m))}{\sigma}$
    \task $\appS{(\all{x} Q(x, g(m)))}{\sigma}$
    \task* $\appS{((\exist{x} Q(x, g(m))) \land \all{y} Q(y, z))}{\sigma}$
    \task $\appS{(\exist{y} Q(y, g(z)) \land \all{x} Q(y, f(x, z)))}{\sigma}$
  \end{tasks}
\end{exercise}
\begin{solution}
  \begin{tasks}
    \task $\appS{Q(x, g(m))}{\sigma} = Q(g(x), g(m))$
    \task Note that $x$ is not fresh for $\sigma$ and we cannot directly apply
    carry out the substitution, even though $y$ never appears in the formula.
    Instead, we do the following.
    \begin{align*}
      \appS{(\all{x} Q(x, g(m)))}{\sigma}
      & = \appS{(\all{x'} Q(x', g(m)))}{\sigma}
      \tag*{\axref{EQ}} \\
      & = \all{x'} \appS{Q(x', g(m))}{(\sigma \update{x'}{x'})}
      \tag*{\axref{SQ}} \\
      & = \all{x'} Q(x', g(m))
      \tag*{\axref{SP}} \\
      & = \all{x} Q(x, g(m))
      \tag*{\axref{EQ}}
    \end{align*}
    \task
    Note that $\sigma\update{x}{x} = \update{y}{x} \update{z}{y}$, which means that
    this updated substitution does not affect the occurrences of $x$ in the third
    step below.
    \begin{align*}
      & \appS{((\exist{x} Q(x, g(m))) \land \all{y} Q(y, z))}{\sigma} \\
      & = \appS{((\exist{x} Q(x, g(m))))}{\sigma} \land \appS{(\all{y} Q(y, z))}{\sigma}
      \tag*{\axref{SC}} \\
      & = (\exist{x} Q(x, g(m))) \land \appS{(\all{y} Q(y, z))}{\sigma}
      \tag*{\axref{SQ}} \\
      & = (\exist{x} Q(x, g(m))) \land \appS{(\all{y'} Q(y', z))}{\sigma}
      \tag*{\axref{EQ}} \\
      & = (\exist{x} Q(x, g(m))) \land (\all{y'} \appS{Q(y', z)}{\sigma})
      \tag*{\axref{SQ}} \\
      & = (\exist{x} Q(x, g(m))) \land (\all{y'} Q(y', y))
      \tag*{\axref{SP}}
    \end{align*}
    \task
    \begin{align*}
      & \appS{(\exist{y} Q(y, g(z)) \land \all{x} Q(y, f(x, z)))}{\sigma} \\
      & = \appS{(\exist{y'} Q(y', g(z)) \land \all{x} Q(y', f(x, z)))}{\sigma}
      \tag*{\axref{EQ}} \\
      & = \exist{y'} \appS{(Q(y', g(z)) \land \all{x} Q(y', f(x, z)))}{\sigma}
      \tag*{\axref{SQ}} \\
      & = \exist{y'} \appS{Q(y', g(z))}{\sigma} \land \appS{(\all{x} Q(y', f(x, z)))}{\sigma}
      \tag*{\axref{SC}} \\
      & = \exist{y'} Q(y', g(y)) \land \appS{(\all{x} Q(y', f(x, z)))}{\sigma}
      \tag*{\axref{SP}} \\
      & = \exist{y'} Q(y', g(y)) \land \appS{(\all{x'} Q(y', f(x', z)))}{\sigma}
      \tag*{\axref{EQ}} \\
      & = \exist{y'} Q(y', g(y)) \land \all{x'} \appS{Q(y', f(x', z))}{\sigma}
      \tag*{\axref{SQ}} \\
      & = \exist{y'} Q(y', g(y)) \land \all{x'} Q(y', f(x', y))
      \tag*{\axref{SP}} \\
      & = \exist{y'} Q(y', g(y)) \land \all{x} Q(y', f(x, y))
      \tag*{\axref{EQ}}
    \end{align*}
    In the last step, we used that $\fresh{x} Q(y', f(x', y))$.
    This step is not strictly necessary, but shows that $\sigma$ does not affect
    the binding of $x$ because no variable is replaced with a term that contains $x$.
  \end{tasks}
\end{solution}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: